#include <iostream>
#include <Python.h>
#include <boost/foreach.hpp>
#include <boost/range/value_type.hpp>
#include <boost/python.hpp>
#include <boost/random.hpp>
#include <vector>
#include <cmath>
#include <algorithm>

typedef std::vector<int> int_vector;
typedef std::vector<long> l_vector;
typedef std::vector<float> f_vector;
typedef std::vector<bool> b_vector;

namespace py = boost::python;

//test function
/*
void print(int_vector a){
  for(int i=0;i<a.size();i++){
    std::cout << a[i] << " " << std::endl;
  }
}
*/

boost::mt19937 _rng;
boost::uniform_real<float> _u(0.0f,1.0f);
boost::variate_generator<boost::mt19937&, boost::uniform_real<float> > uniform(_rng, _u);

//end is exclusive
long randomBetween(long start, long end){
  boost::random::uniform_int_distribution<> dist(start,end-1);
  return dist(_rng);
}

template <typename T>
void copyListIntoArray(boost::python::list &source, std::vector<T> &target){
  long length = boost::python::len(source);
  target.resize(length);
  for(long i=0; i<length; i++){
    target[i] = boost::python::extract<T>(source[i]);
    //std::cout << target.back() << " ";
  }
  //std::cout << std::endl;
}

template <typename T>
void printArray(std::vector<T> &target){
  for(long i=0; i<target.size(); i++){
    std::cout << target[i] << " ";
  }
  std::cout << ";" << std::endl;
}

class ExclusiveBuffer{
public:
  void add_value(long val);
  bool find_value(long query);
  void erase_value(long target);
  long choice();
  long size();

  l_vector array;
};

void ExclusiveBuffer::add_value(long val){
  l_vector::iterator pos = std::lower_bound(this->array.begin(), this->array.end(), val);
  if(pos==this->array.end() || *pos!=val){
    this->array.insert(pos,val);
  }
}

bool ExclusiveBuffer::find_value(long query){
  l_vector::iterator pos = std::lower_bound(this->array.begin(), this->array.end(), query);
  return (pos!=this->array.end() && *pos == query);
}

void ExclusiveBuffer::erase_value(long target){
  l_vector::iterator pos = std::lower_bound(this->array.begin(), this->array.end(), target);
  if(pos!=this->array.end() && *pos==target){
    this->array.erase(pos);
  }
}

long ExclusiveBuffer::choice(){
  long pos = randomBetween(0, this->array.size());
  return this->array[pos];
}

long ExclusiveBuffer::size(){
  return this->array.size();
}

void test(){
  ExclusiveBuffer buffer;
  buffer.add_value(3);
  buffer.add_value(1);
  buffer.add_value(5);

  std::cout << "Test" << std::endl;

  std::cout << buffer.size() << std::endl;

  std::cout << buffer.find_value(0) << std::endl;
  std::cout << buffer.find_value(1) << std::endl;

  buffer.erase_value(1);
  std::cout << buffer.size() << std::endl;

  std::cout << buffer.find_value(1) << std::endl;

}

class MCSAT_DataObject{
public:
  long nGndAtom;
  long nBlock;
  long nClause;
  long nGFormula;
  int nFormula;

  //set at begining
  l_vector atomIdx2BlockIdx;
  std::vector<l_vector> blockIdx2AtomIdx;
  //set when converting formulas into clauses
  l_vector gndFormula_start;
  l_vector gndFormula_end;
  f_vector gndFormula_weight;
  int_vector formulaIdx;
  l_vector gndAtomIdxs;
  b_vector gndAtomNegs;
  l_vector clause_start;
  l_vector clause_end;
  std::vector<l_vector> atom2clauseIdx;
  std::vector<b_vector> atom2clauseNeg;
  //set when setting evidence
  l_vector evidence;
  b_vector untrivial_clauses;
  b_vector evidenceBlock;

  l_vector M;

  l_vector satisfied_count;

  //use at begining
  void initialize(py::list &atomIdx2BlockIdx, py::list &blockIdx2AtomIdx, int nFormula);
  //use when converting formulas into clauses
  void setClauseInformation(
    py::list &gndFormula_start,
    py::list &gndFormula_end,
    py::list &gndFormula_weight,
    py::list &formulaIdx,
    py::list &gndAtomIdxs,
    py::list &gndAtomNegs,
    py::list &clause_start,
    py::list &clause_end,
    py::list &atom2clauseIdx,
    py::list &atom2clauseNeg
  );
  void setClauseInformationOnlyWeight(py::list &gndFormula_weight);
  //use when setting evidence
  void setEvidence(py::dict &evid);

  void print();
};

void MCSAT_DataObject::initialize(py::list &atomIdx2BlockIdx, py::list &blockIdx2AtomIdx, int nFormula){
  this->nGndAtom = py::len(atomIdx2BlockIdx);
  this->nBlock = py::len(blockIdx2AtomIdx);
  this->nFormula = nFormula;

  //for atomIdx2BlockIdx
  copyListIntoArray<long>(atomIdx2BlockIdx, this->atomIdx2BlockIdx);

  //for blockIdx2AtomIdx
  this->blockIdx2AtomIdx.resize(this->nBlock);
  for(long i=0; i<this->nBlock; i++){
    std::vector<long> tmp(0);
    py::list tmpLst = py::extract<py::list>(blockIdx2AtomIdx[i]);
    copyListIntoArray<long>(tmpLst, tmp);
    this->blockIdx2AtomIdx[i] = tmp;
  }

}

void MCSAT_DataObject::setClauseInformation(
  py::list &gndFormula_start,
  py::list &gndFormula_end,
  py::list &gndFormula_weight,
  py::list &formulaIdx,
  py::list &gndAtomIdxs,
  py::list &gndAtomNegs,
  py::list &clause_start,
  py::list &clause_end,
  py::list &atom2clauseIdx,
  py::list &atom2clauseNeg
){
  this->nGFormula = py::len(gndFormula_start);
  this->nClause = py::len(clause_start);

  copyListIntoArray<long>(gndFormula_start, this->gndFormula_start);
  copyListIntoArray<long>(gndFormula_end, this->gndFormula_end);
  copyListIntoArray<float>(gndFormula_weight, this->gndFormula_weight);
  copyListIntoArray<int>(formulaIdx, this->formulaIdx);
  copyListIntoArray<long>(gndAtomIdxs, this->gndAtomIdxs);
  copyListIntoArray<bool>(gndAtomNegs, this->gndAtomNegs);
  copyListIntoArray<long>(clause_start, this->clause_start);
  copyListIntoArray<long>(clause_end, this->clause_end);

  this->atom2clauseIdx.resize(this->nGndAtom);
  this->atom2clauseNeg.resize(this->nGndAtom);
  for(long i=0;i<this->nGndAtom;i++){
    std::vector<long> tmpIdx(0);
    py::list idxLst = py::extract<py::list>(atom2clauseIdx[i]);
    copyListIntoArray<long>(idxLst, tmpIdx);
    this->atom2clauseIdx[i] = tmpIdx;

    std::vector<bool> tmpNeg(0);
    py::list negLst = py::extract<py::list>(atom2clauseNeg[i]);
    copyListIntoArray<bool>(negLst, tmpNeg);
    this->atom2clauseNeg[i] = tmpNeg;
  }

}

void MCSAT_DataObject::setClauseInformationOnlyWeight(py::list &gndFormula_weight){
  copyListIntoArray<float>(gndFormula_weight, this->gndFormula_weight);
}


void MCSAT_DataObject::setEvidence(py::dict &evid){
  long evid_len = py::len(evid);
  py::list keys = evid.keys();

  this->evidence = l_vector(this->nGndAtom, -1);
  this->untrivial_clauses = b_vector(this->nClause, false);
  this->evidenceBlock = b_vector(this->nBlock, false);

  //std::cout << this->nBlock << " " << this->nClause << " " << this->nGndAtom << std::endl;
  //std::cout << this->evidence.size() << " " << this->untrivial_clauses.size() << " " << this->evidenceBlock.size() << std::endl;

  long gndAtomIdx, blockIdx ,otherAtomIdx, clauseIdx, j;
  bool tv, ifNegated;
  l_vector otherAtoms, clauseIdxs;
  b_vector ifNegateds;

  for(long i=0;i<evid_len;i++){
    gndAtomIdx = py::extract<long>(keys[i]);
    tv = py::extract<bool>(evid[gndAtomIdx]);

    this->evidence[gndAtomIdx] = tv ? 1 : 0;

    blockIdx = this->atomIdx2BlockIdx[gndAtomIdx];

    if(tv){
      otherAtoms = this->blockIdx2AtomIdx[blockIdx];
      for(j=0; j<otherAtoms.size(); j++){
        otherAtomIdx = otherAtoms[j];
        if(otherAtomIdx==gndAtomIdx) continue;

        this->evidence[otherAtomIdx] = 0;
      }

      this->evidenceBlock[blockIdx] = true;
    }else{
      //std::cout << this->blockIdx2AtomIdx.size();
      otherAtoms = this->blockIdx2AtomIdx[blockIdx];

      if(otherAtoms.size()==1){
        this->evidenceBlock[blockIdx] = true;
      }
    }

    clauseIdxs = this->atom2clauseIdx[gndAtomIdx];
    ifNegateds = this->atom2clauseNeg[gndAtomIdx];

    for(j=0; j<clauseIdxs.size(); j++){
      clauseIdx = clauseIdxs[j];
      ifNegated = ifNegateds[j];
      if(tv^ifNegated){
        this->untrivial_clauses[clauseIdx] = true;
      }
    }
  }

  //printArray(this->evidence);
  //printArray(this->untrivial_clauses);
  //printArray(this->evidenceBlock);
}

void MCSAT_DataObject::print(){
  std::cout << "============ MCSAT DataObject ============" << std::endl;

  std::cout << "atomIdx2BlockIdx" << std::endl;
  printArray(this->atomIdx2BlockIdx);

  std::cout << "blockIdx2AtomIdx" << std::endl;
  for(long i = 0; i<this->nBlock; i++){
    printArray(this->blockIdx2AtomIdx[i]);
  }

  std::cout << "gndFormula_start" << std::endl;
  printArray(this->gndFormula_start);

  std::cout << "gndFormula_end" << std::endl;
  printArray(this->gndFormula_end);

  std::cout << "gndFormula_weight" << std::endl;
  printArray(this->gndFormula_weight);

  std::cout << "gndAtomIdx" << std::endl;
  printArray(this->gndAtomIdxs);

  std::cout << "gndAtomNegs" << std::endl;
  printArray(this->gndAtomNegs);

  std::cout << "clause_start" << std::endl;
  printArray(this->clause_start);

  std::cout << "clause_end" << std::endl;
  printArray(this->clause_end);

  std::cout << "atom2clauseIdx" << std::endl;
  for(long i=0; i<this->nGndAtom; i++){
    printArray(this->atom2clauseIdx[i]);
  }

  std::cout << "atom2clauseNeg" << std::endl;
  for(long i=0; i<this->nGndAtom; i++){
    printArray(this->atom2clauseNeg[i]);
  }

  std::cout << "evidence" << std::endl;
  printArray(this->evidence);

  std::cout << "untrivial_clauses" << std::endl;
  printArray(untrivial_clauses);

  std::cout << "evidenceBlock" << std::endl;
  printArray(evidenceBlock);
}

l_vector const& sampleClauseSet(b_vector &state, MCSAT_DataObject &obj){
  obj.M = std::vector<long>(0);

  obj.satisfied_count = l_vector(obj.nFormula, 0);


  // l_vector count = l_vector(obj.nFormula, 0);
  // std::vector<float> f_weight = std::vector<float>(obj.nFormula, 0.0f);
  for(long i=0;i<obj.nGFormula;i++){
    long sf = obj.gndFormula_start[i];
    long ef = obj.gndFormula_end[i];
    float w = obj.gndFormula_weight[i];

    bool flag = true;
    for(long i_clause=sf; i_clause<ef; i_clause++){
      long sc = obj.clause_start[i_clause];
      long ec = obj.clause_end[i_clause];
      bool flag_clause = false;
      for(long i_atom=sc; i_atom<ec; i_atom++){
        if(state[obj.gndAtomIdxs[i_atom]] ^ obj.gndAtomNegs[i_atom]){
          flag_clause = true;
          break;
        }
      }
      if(!flag_clause){
        flag = false;
        break;
      }
    }

    // f_weight[obj.formulaIdx[i]] = w;
    if(flag){
      //when satisfied
      obj.satisfied_count[obj.formulaIdx[i]] = obj.satisfied_count[obj.formulaIdx[i]] + 1;

      if(uniform() < 1.0 - std::exp(-w)){
        for(long i_clause=sf; i_clause<ef; i_clause++){
          //omit untrivially satisfied clauses
          if(obj.untrivial_clauses[i_clause]){
            continue;
          }

          obj.M.push_back(i_clause);
        }
      }
      // else{
      //   count[obj.formulaIdx[i]] = count[obj.formulaIdx[i]] + 1;
      // }
    }
  }
  // std::cout << "not satisfied count: "<< std::endl;
  // printArray(count);
  // std::cout << "satisfied count: " << std::endl;
  // printArray(obj.satisfied_count);
  // std::cout << "formula_weight" << std::endl;
  // printArray(f_weight);
  //printArray(obj.M);
  return obj.satisfied_count;
}

void randominitializeState(b_vector &state, MCSAT_DataObject &obj){
  l_vector atoms, unknown_pos;
  for(long i=0; i<obj.nBlock; i++){
      atoms = obj.blockIdx2AtomIdx[i];

      if(atoms.size()==1 && obj.evidence[atoms[0]]==-1){
        state[atoms[0]] = (uniform() < 0.5);
      }
      else{
        unknown_pos = l_vector(0);
        for(long j=0;j<atoms.size();j++){
          if(obj.evidence[atoms[j]]==-1){
            unknown_pos.push_back(j);
            state[atoms[j]] = false;
          }
        }

        if(unknown_pos.size()>0){
          state[atoms[unknown_pos[randomBetween(0,unknown_pos.size())]]] = true;
        }
      }
  }
}

inline bool anyOtherContribution(b_vector &state, MCSAT_DataObject &obj, long i_clause, long atomIdx){
  for(long i_atom=obj.clause_start[i_clause]; i_atom < obj.clause_end[i_clause]; i_atom++){
    long otherAtomIdx = obj.gndAtomIdxs[i_atom];
    bool otherIfNegated = obj.gndAtomNegs[i_atom];
    if((state[otherAtomIdx] ^ otherIfNegated)&&(otherAtomIdx!=atomIdx))
      return true;
  }
  return false;
}


//NOTE: cand must be set empty
void calcUnsatisfiedClauses(b_vector &state, MCSAT_DataObject &obj, ExclusiveBuffer &cand){
  for(long i=0;i<obj.M.size();i++){
    long i_clause = obj.M[i];
    if(obj.untrivial_clauses[i_clause]){
      continue;
    }
    if(anyOtherContribution(state, obj, i_clause, -1)){
      continue;
    }
    cand.add_value(i_clause);
  }
}

void flip_subroutine(b_vector &state, MCSAT_DataObject &obj, ExclusiveBuffer &cand, ExclusiveBuffer &Mmap, long targetIdx, long &delta){
  l_vector clauseIdxs = obj.atom2clauseIdx[targetIdx];
  b_vector clauseNegs = obj.atom2clauseNeg[targetIdx];
  for(long i=0;i<clauseIdxs.size();i++){
    long i_clause = clauseIdxs[i];
    bool ifNeg = clauseNegs[i];

    if(!Mmap.find_value(i_clause)){
      continue;
    }

    bool already_unsatisfied = cand.find_value(i_clause);
    if(anyOtherContribution(state, obj, i_clause, -1)){
      if(already_unsatisfied){
        cand.erase_value(i_clause);
        delta -= 1;
      }
    }
    else{
      if(!already_unsatisfied){
        cand.add_value(i_clause);
        delta += 1;
      }
    }
  }
}

//set secondIdx -1 when you dont have to flip second atom
long flip(b_vector &state, MCSAT_DataObject &obj, ExclusiveBuffer &cand, ExclusiveBuffer &Mmap, long atomIdx, long secondIdx){
  long delta = 0;

  //flip
  state[atomIdx] = state[atomIdx] ^ true;
  if(secondIdx >= 0){
    state[secondIdx] = state[secondIdx] ^ true;
  }
  //std::cout << "check delta " << std::endl;
  //std::cout << delta << std::endl;
  flip_subroutine(state, obj, cand, Mmap, atomIdx, delta);
  //std::cout << delta << std::endl;


  if(secondIdx < 0){
    return delta;
  }

  flip_subroutine(state, obj, cand, Mmap, secondIdx, delta);

  return delta;
}

inline long walkSATMove_subroutine(b_vector &state, MCSAT_DataObject &obj, ExclusiveBuffer &cand, ExclusiveBuffer &Mmap, long atomIdx){
  //std::cout << "evaluate candidate flip: " << atomIdx << std::endl;
  long cost = 0;
  l_vector clauseIdxs = obj.atom2clauseIdx[atomIdx];
  b_vector clauseNegs = obj.atom2clauseNeg[atomIdx];

  for(long i=0;i<clauseIdxs.size();i++){
    long clauseIdx = clauseIdxs[i];
    bool clauseNeg = clauseNegs[i];
    //std::cout << "flip clause? "<<clauseIdx << " " << (clauseNeg ? "True" : "False") << std::endl;
    if(!Mmap.find_value(clauseIdx)){
      //std::cout << "this is not in M" << std::endl;
      continue;
    }
    //std::cout << "state - neg - contribution" << state[atomIdx] << " " << clauseNeg << " " << anyOtherContribution(state, obj, clauseIdx, atomIdx) << std::endl;
    if((state[atomIdx] ^ clauseNeg) && (!anyOtherContribution(state, obj, clauseIdx, atomIdx))){
      //std::cout << "flip this atom cause unsatisfied clause" << std::endl;
      cost += 1;
    }
  }
  return cost;
}

void walkSATMove(b_vector &state, MCSAT_DataObject &obj, ExclusiveBuffer &cand, ExclusiveBuffer &Mmap){
  long flip_clause = cand.choice();

  //std::cout << "selected clause: " << flip_clause << std::endl;

  long bestFlipAtom = -1;
  long bestSecondAtom = -1;
  long bestValue = 32767;//max value of int
  for(long i=obj.clause_start[flip_clause]; i<obj.clause_end[flip_clause];i++){
    long atomIdx = obj.gndAtomIdxs[i];

    //std::cout << "selected ground atom: " << atomIdx << std::endl;

    if(obj.evidence[atomIdx] != -1){
      //std::cout << "this atom is evidence. skip" << std::endl;
      continue;
    }

    //cost of this atom
    long cost = walkSATMove_subroutine(state, obj, cand, Mmap, atomIdx);

    long blockIdx = obj.atomIdx2BlockIdx[atomIdx];
    l_vector blockAtomIdxs = obj.blockIdx2AtomIdx[blockIdx];

    //if there is no other block members, evaluate cost
    if(blockAtomIdxs.size()==1){
      //std::cout << "this atom has no block" << std::endl;
      //std::cout << "cost" << cost << std::endl;
      if(cost < bestValue || (cost==bestValue && uniform() < 0.5)){
        bestFlipAtom = atomIdx;
        bestSecondAtom = -1;
        bestValue = cost;
      }
    }
    //we have to consider other block member's flip
    else{
      l_vector secondCandidates;
      if(state[atomIdx]){
        secondCandidates = blockAtomIdxs;
      }
      else{
        for(long j=0;j<blockAtomIdxs.size();j++){
          if(state[blockAtomIdxs[j]]){
            secondCandidates.push_back(blockAtomIdxs[j]);
            break;
          }
        }
      }

      for(long j=0;j<secondCandidates.size();j++){
        long secondAtomIdx = secondCandidates[j];
        if(secondAtomIdx==atomIdx){
          continue;
        }

        long second_cost = walkSATMove_subroutine(state,obj,cand,Mmap,secondAtomIdx);

        if(cost+second_cost < bestValue || (cost+second_cost==bestValue && uniform() < 0.5)){
          bestFlipAtom = atomIdx;
          bestSecondAtom = secondAtomIdx;
          bestValue = cost + second_cost;
        }
      }
    }
  }
  //flip
  long delta = flip(state, obj, cand, Mmap, bestFlipAtom, bestSecondAtom);
  //std::cout << "cost: " << bestValue << " delta: " << delta << " cand: " << cand.size() << std::endl;

  //int tmp;
  //std::cin >> tmp;
}

void SAMove(b_vector &state, MCSAT_DataObject &obj, ExclusiveBuffer &cand, ExclusiveBuffer &Mmap, float T){
  long flipAtom = -1;
  long secondAtom = -1;
  while(true){
    long blockIdx = randomBetween(0,obj.nBlock);
    if(!obj.evidenceBlock[blockIdx]){
      l_vector blockAtoms = obj.blockIdx2AtomIdx[blockIdx];
      if(blockAtoms.size()==1){
          flipAtom = blockAtoms[0];
          goto FLIP;
      }
      else{
        l_vector cand_atom;
        long TrueOne = -1;
        for(long i=0;i<blockAtoms.size();i++){
          if(obj.evidence[blockAtoms[i]]==-1){
            if(state[blockAtoms[i]]){
              TrueOne = blockAtoms[i];
            }
            else{
              cand_atom.push_back(blockAtoms[i]);
            }
          }
        }
        if(cand_atom.size()>0){
          flipAtom = TrueOne;
          secondAtom = cand_atom[randomBetween(0,cand_atom.size())];
          goto FLIP;
        }
      }
    }
    else{
      return;
    }
    continue;
    FLIP:
    //std::cout << "flipAtom" << std::endl;
    //std::cout << flipAtom << std::endl;
    //std::cout << secondAtom << std::endl;
    bool b1 = state[flipAtom];
    bool b2;
    if(secondAtom>=0){
      b2 = state[secondAtom];
    }

    long delta = flip(state, obj, cand, Mmap, flipAtom, secondAtom);
    float _p = std::exp(-delta/T);
    //std::cout << "delta " << delta <<  " p " << _p << std::endl;

    if(!(uniform() < _p)){
      //cancel
      state[flipAtom] = b1;
      if(secondAtom >= 0){
        state[secondAtom] = b2;
      }
    }

    break;
  }
}

b_vector const& sampleSAT(b_vector &state, MCSAT_DataObject &obj, float p, float T, long maxIter, bool verbose){
  //take back up in case when sampleSAT fails to find solution
  b_vector state_bu(state.size());
  long state_size =  state.size();
  for(long i=0;i<state_size;i++){
    state_bu[i] = state[i];
  }

  ExclusiveBuffer Mmap;
  long Msize = obj.M.size();
  for(long i=0;i<Msize;i++){
    //std::cout << obj.M[i] << "in M" << std::endl;
    Mmap.add_value(obj.M[i]);
  }

  //std::cout << "M size" << obj.M.size() << std::endl;

  ExclusiveBuffer cand;

  //NOTE: TEMPORARILY REMOVED
  randominitializeState(state, obj);

  //std::cout << "state" << std::endl;
  //printArray(state);

  calcUnsatisfiedClauses(state, obj, cand);

  //std::cout << "initial unsatisfied clauses" << cand.size() << std::endl;

  long i_iter = 0;
  long init_remaining = cand.size();
  while(true){
    if(cand.size() == 0 || i_iter > maxIter){
      if(i_iter > maxIter){
        if(verbose){
          std::cout << "reached Max iter" << std::endl;
          std::cout << "final candidate num" << cand.size() << std::endl;
        }
        long state_size =  state.size();
        for(long i=0;i<state_size;i++){
          state[i] = state_bu[i];
        }
      }else{
        if(verbose){
          std::cout << "SampleSAT successfully found solution in " << i_iter << " iterations" << std::endl;
        }
      }
      //std::cout << i_iter << std::endl;
      break;
    }
    i_iter += 1;

    if(i_iter%100==0 && verbose){
      std::cout << "Iteration: " << i_iter << "\tRemaining: " << cand.size() << "\tout of " << init_remaining << "\r" << std::flush;
    }

    //std::cout << "cand: " << cand.size() << std::endl;
    //std::cin >> i_iter;

    if(uniform()<p){
      //std::cout << "WalkSAT" << std::endl;
      walkSATMove(state, obj, cand, Mmap);
    }
    else{
      //std::cout << "SA Move" << std::endl;
      SAMove(state, obj, cand, Mmap, T);
    }
  }

  return state;
}

template<typename T_>
class vector_to_pylist_converter {
public:
    typedef T_ native_type;

    static PyObject* convert(native_type const& v) {
        py::list retval;
        BOOST_FOREACH(typename boost::range_value<native_type>::type i, v)
        {
            retval.append(py::object(i));
        }
        return py::incref(retval.ptr());
    }
};

template<typename T_>
class pylist_to_vector_converter {
public:
    typedef T_ native_type;

    static void* convertible(PyObject* pyo) {
        if (!PySequence_Check(pyo))
            return 0;

        return pyo;
    }

    static void construct(PyObject* pyo, boost::python::converter::rvalue_from_python_stage1_data* data)
    {
        native_type* storage = new(reinterpret_cast<py::converter::rvalue_from_python_storage<native_type>*>(data)->storage.bytes) native_type();
        for (py::ssize_t i = 0, l = PySequence_Size(pyo); i < l; ++i) {
            storage->push_back(
                py::extract<typename boost::range_value<native_type>::type>(
                    PySequence_GetItem(pyo, i)));
        }
        data->convertible = storage;
    }
};

BOOST_PYTHON_MODULE(mcsat_cpp){
  using namespace boost::python;

  class_<MCSAT_DataObject>("MCSAT_DataObject")
    .def("initialize", &MCSAT_DataObject::initialize)
    .def("setClauseInformation", &MCSAT_DataObject::setClauseInformation)
    .def("setClauseInformationOnlyWeight", &MCSAT_DataObject::setClauseInformationOnlyWeight)
    .def("setEvidence", &MCSAT_DataObject::setEvidence)
    .def("printData", &MCSAT_DataObject::print);

  //ここでb_vector const&の関数ポインタにキャストしておかないとコンパイル通らない
  def("sampleClauseSet", (l_vector const&(*)(b_vector const&, MCSAT_DataObject&))&sampleClauseSet, return_value_policy<copy_const_reference>());
  def("sampleSAT", (b_vector const&(*)(b_vector const&, MCSAT_DataObject&, float, float, long, bool))&sampleSAT, return_value_policy<copy_const_reference>());

  def("test", &test);

  //def("pri", &print);
  to_python_converter<b_vector, vector_to_pylist_converter<b_vector > >();
  converter::registry::push_back(
      &pylist_to_vector_converter<b_vector >::convertible,
      &pylist_to_vector_converter<b_vector >::construct,
      boost::python::type_id<b_vector >());

  to_python_converter<l_vector, vector_to_pylist_converter<l_vector > >();
  converter::registry::push_back(
          &pylist_to_vector_converter<l_vector >::convertible,
          &pylist_to_vector_converter<l_vector >::construct,
          boost::python::type_id<l_vector >());
}
