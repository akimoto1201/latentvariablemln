# coding: UTF-8

import os
import subprocess, string, re
from pyparsing import Literal, Word, Group, Combine, alphanums,Suppress,ZeroOrMore, ParseException

import numpy as np

from base import MLN

import config

###################
#Alchemyのコマンドを実行する関数
###################
ALCHEMY_BIN = config.ALCHEMY_BIN

def learnWt_Alchemy(progFile, evidFile, outFile, non_evidence, dNumIter=None):
    cmd = [ALCHEMY_BIN+'learnwts', '-d', '-dNewton',  '-i', progFile, '-o', outFile, '-t', evidFile, '-ne', ','.join(non_evidence) , '-noAddUnitClauses', '-noPrior']
    if type(dNumIter) != type(None):
        cmd += ['-dNumIter', str(dNumIter)]
    command = subprocess.list2cmdline(cmd)
    print command

    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    while True:
        line = p.stdout.readline()
        if len(line)>1:
            print line[:-1]
        if not line and p.poll() is not None:
            break

def inferMAP_Alchemy(progFile, evidFile, queryFile, outFile):
    cmd = [ALCHEMY_BIN+'infer', '-a', '-i', progFile, '-r', outFile, '-e', evidFile, '-f', queryFile]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    while True:
        line = p.stdout.readline()
        if len(line)>1:
            print line[:-1]
        if not line and p.poll() is not None:
            break

def inferMarginal_Alchemy(progFile, evidFile, queryFile, outFile):
    cmd = [ALCHEMY_BIN+'infer', '-ms', '-i', progFile, '-r', outFile, '-e', evidFile, '-f', queryFile]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    while True:
        line = p.stdout.readline()
        if len(line)>1:
            print line[:-1]
        if not line and p.poll() is not None:
            break

###################
#Alchemyによる出力ファイルのパーサ
###################
def convertLearnedWeightIntoMLN_Alchemy(resultFile):
    u"""

    """
    num = '0123456789'
    word = alphanums+"_"
    number = Combine(ZeroOrMore(Word('-')) + Word(num) + ZeroOrMore("."+Word(num)))
    null = Literal('null')
    predicate = Group(Group(ZeroOrMore('!')+Word(word))+Suppress('(')+Group(Word(word)+ZeroOrMore(Suppress(',')+Word(word)))+Suppress(')')) ^ null
    formula = number + predicate + ZeroOrMore(Suppress('v')+predicate)

    weights = []
    not_lsts = []
    pred_lsts = []
    var_lsts = []


    h = open(resultFile,'r')
    for line in h.readlines():
        line = line.translate(string.maketrans("\t"," "),"\r\n")#改行文字削除

        if len(line)>0:
            parse_result = None
            try:
                parse_result = formula.parseString(line)
            except ParseException:
                continue
            if parse_result[1]=='null':
                continue
            weight = float(parse_result[0])
            not_lst = []
            pred_lst = []
            var_lst = []
            variable_idx = {}
            counter = -1
            for atom in parse_result[1:]:
                counter += 1
                #否定でない時
                if len(atom[0])==1:
                    not_lst.append(0)
                    pred_lst.append(atom[0][0])
                #否定の時
                else:
                    not_lst.append(1)
                    pred_lst.append(atom[0][1])
                #各変数について
                for i in range(len(atom[1])):
                    var = atom[1][i]
                    #定数の時
                    try:
                        var = int(var)
                        var_lst.append((counter,i,var))
                        continue
                    #変数の時
                    except ValueError:
                        if variable_idx.keys().count(var)==0:
                            variable_idx[var] = len(var_lst)
                            var_lst.append([])
                        var_lst[variable_idx[var]].append((counter,i))
            weights.append(weight)
            not_lsts.append(not_lst)
            pred_lsts.append(pred_lst)
            var_lsts.append(var_lst)
    return weights, not_lsts, pred_lsts, var_lsts

##############################
def learnMLNviaAlchemy(mln, predicates, state, **args):
    u"""
    与えられたMLNインスタンスを元に学習を行う
    stateは一次元array

    predicates -- 含まれる述語の名前の文字列リスト
    """
    assert isinstance(mln, MLN)

    file_num = np.random.randint(0,100000)
    evid_file = '_tmp_evid_%d.db'%file_num
    prog_file = '_tmp_prog_%d.mln'%file_num
    out_file = '_tmp_out_%d.mln'%file_num

    #program file
    h = open(prog_file, 'w')
    h.write(mln.prog_str)
    h.close()

    #evidence file
    EVID = ''
    for i in xrange(mln.getNGndAtom()):
        EVID += '!' if (not state[i]) else ''
        EVID += str(mln.getGndAtom(i)) + '\n'
    h = open(evid_file, 'w')
    h.write(EVID)
    h.close()

    #learn weight
    learnWt_Alchemy(prog_file, evid_file, out_file, predicates, **args)

    #parse learned weight
    weights, nl, pl, vl = convertLearnedWeightIntoMLN_Alchemy(out_file)

    if len(weights) == 0:
        #SOMETHING HAPPEND!!!
        raise Exception("Alchemy returned empty weights!! may be nan happend?")

    #input('fin')

    #delete temporary files
    os.remove(evid_file)
    os.remove(prog_file)
    os.remove(out_file)

    return weights
