#coding: UTF-8

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm

from sklearn.cluster import KMeans
from sklearn.metrics import f1_score

class Switcher(object):
    def __init__(self, V, K, I, alpha):
        #次元の初期設定
        self.V = V #潜在変数の種類
        self.K = K #観測モデルのクラスタ数
        self.I = I #データの数

        #事前分布のパラメータ
        self.alpha = alpha

        #クラスタの初期化
        self.c = np.random.randint(0, self.K, size=self.I)
        #カウンタ
        self.n = np.zeros((self.V, self.K)).astype(int)

    def initN(self, z):
        #カウンタ初期化
        self.n[:,:] = 0
        for i in xrange(self.I):
            self.n[z[i], self.c[i]] += 1

    def sampleC(self, logx, z):
        u"""
        [Keyword arguments]
            logx -- (n_data, n_cluster)のndarray 定数を除く対数尤度を格納している
            z -- (n_data,)のint ndarrayで、各データの潜在変数を格納している
        """
        #正規化された各データの観測モデル側の確率を計算する
        px = np.exp(logx)
        px = px / np.sum(px, axis=1, keepdims=True)

        #Gibbsサンプリング
        for i in xrange(self.I):
            c_i = self.c[i]
            z_i = z[i]

            self.n[z_i, c_i] -= 1

            mean_theta = self.n[z_i] + self.alpha[z_i]
            mean_theta = mean_theta / np.sum(mean_theta)

            prob = px[i] * mean_theta
            prob = prob / np.sum(prob)

            new_c_i = np.random.choice(self.K, p=prob)

            self.n[z_i, new_c_i] += 1

            self.c[i] = new_c_i

    def sampleZ(self, z, logz):
        u"""
        [Keyword arguments]
            z -- (n_data,)のint ndarray
            logz -- (n_data, n_zdim) 定数を除くzの対数尤度を格納している
        """
        pz = np.exp(logz)
        pz = pz / np.sum(pz, axis=1, keepdims=True)

        #Gibbsサンプリング
        for i in xrange(self.I):
            c_i = self.c[i]
            z_i = z[i]

            self.n[z_i, c_i] -= 1

            mean_theta = (self.n[:,c_i] + self.alpha[np.newaxis, c_i]) / np.sum(self.n + self.alpha[np.newaxis,:], axis=1)
            mean_theta /= np.sum(mean_theta)

            prob = pz[i] * mean_theta
            prob /= np.sum(prob)

            new_z_i = np.random.choice(self.V, p=prob)

            self.n[new_z_i, c_i] += 1

            z[i] = new_z_i

        return z

    def sampleZ2(self, z, logz, i):
        u"""
        iのところだけサンプルする
        [Keyword arguments]
            z -- (n_data,)のint ndarray
            logz -- (n_zdim,) 定数を除くzの対数尤度を格納している
        """
        #Gibbsサンプリング
        c_i = self.c[i]
        z_i = z[i]

        pz = np.exp(logz)
        pz = pz / np.sum(pz)

        self.n[z_i, c_i] -= 1

        mean_theta = (self.n[:,c_i] + self.alpha[np.newaxis, c_i]) / np.sum(self.n + self.alpha[np.newaxis,:], axis=1)
        mean_theta /= np.sum(mean_theta)

        prob = pz * mean_theta
        prob /= np.sum(prob)

        new_z_i = np.random.choice(self.V, p=prob)

        self.n[new_z_i, c_i] += 1

        z[i] = new_z_i

        return z

    def calcExpectedTheta(self):
        tmp = self.n[:,:] + self.alpha[np.newaxis,:]
        tmp = tmp / np.sum(tmp, axis=1, keepdims=True)

        return tmp #(n_zdim, n_cls)

class SwitcherTest(object):
    def __init__(self, V, K, I):
        #次元の初期設定
        self.V = V #潜在変数の種類
        self.K = K #観測モデルのクラスタ数
        self.I = I #データの数

        #クラスタの初期化
        self.c = np.random.randint(0, self.K, size=self.I)
        #カウンタ
        self.n = np.zeros((self.V, self.K)).astype(int)

    def initN(self, z):
        #カウンタ初期化
        self.n[:,:] = 0
        for i in xrange(self.I):
            self.n[z[i], self.c[i]] += 1

    def sampleC(self, logx, z, theta):
        u"""
        [Keyword arguments]
            logx -- (n_data, n_cluster)のndarray 定数を除く対数尤度を格納している
            z -- (n_data,)のint ndarrayで、各データの潜在変数を格納している
            theta -- (n_zdim, n_cluster)
        """
        #正規化された各データの観測モデル側の確率を計算する
        px = np.exp(logx)
        px = px / np.sum(px, axis=1, keepdims=True)

        #Gibbsサンプリング
        for i in xrange(self.I):
            c_i = self.c[i]
            z_i = z[i]

            self.n[z_i, c_i] -= 1

            mean_theta = theta[z_i]

            prob = px[i] * mean_theta
            prob = prob / np.sum(prob)

            new_c_i = np.random.choice(self.K, p=prob)

            self.n[z_i, new_c_i] += 1

            self.c[i] = new_c_i


    def sampleZ2(self, z, logz, i, theta):
        u"""
        iのところだけサンプルする
        [Keyword arguments]
            z -- (n_data,)のint ndarray
            logz -- (n_zdim,) 定数を除くzの対数尤度を格納している
            theta -- (n_zdim, n_cluster)
        """
        #Gibbsサンプリング
        c_i = self.c[i]
        z_i = z[i]

        pz = np.exp(logz)
        pz = pz / np.sum(pz)

        self.n[z_i, c_i] -= 1

        mean_theta = theta[:,c_i]
        mean_theta /= np.sum(mean_theta)

        prob = pz * mean_theta
        prob /= np.sum(prob)
        #print theta.shape, mean_theta.shape, pz.shape, prob.shape

        new_z_i = np.random.choice(self.V, p=prob)

        self.n[new_z_i, c_i] += 1

        z[i] = new_z_i

        return z

class ModelMultiGaussians(object):
    def __init__(self, mu_init, sigma_init):
        u"""
        [Keyword arguments]
            mu_init -- (n_cluster, n_dim)
            sigma_init -- (n_cluster, n_dim, n_dim)
        """
        self.mu = mu_init
        self.sigma = sigma_init

    def logL(self, X):
        u"""
        X -- (n_data, n_dim)
        [Return]
            L -- (n_data, n_cls) 各クラスタでの対数尤度 共通の定数項は省いてある
        """
        detsigma = np.linalg.det(self.sigma)#(n_cls,)

        delta = X[:,np.newaxis,:] - self.mu[np.newaxis,:,:] #(n_data, n_cls, n_dim)

        L = -0.5 * np.sum(
                delta[:,:,:,np.newaxis] *
                np.linalg.inv(self.sigma)[np.newaxis,:,:,:] *
                delta[:,:,np.newaxis,:] #(n_data, n_cls, n_dim, n_dim)
                , axis=(2,3)
            ) -0.5 * np.log(detsigma)[np.newaxis,:]

        return L

    def train(self, X, Y):
        u"""
        [Keyword arguments]
            X -- (n_data, n_dim)
            y -- (n_data,) 各データのラベル
        """
        n_cls = self.mu.shape[0]

        self.mu = np.array([np.mean(X[Y==i], axis=0) for i in range(n_cls)])
        self.sigma = np.array([np.cov(X[Y==i].T) for i in range(n_cls)])
        if self.sigma.ndim==1:
            self.sigma = self.sigma.reshape((-1,1,1))

if __name__=="__main__":
    #DATA PREPARATION
    mus = [[np.cos(theta), np.sin(theta)] for theta in np.linspace(0.0, 2*np.pi, 6, endpoint=False)]
    sigmas = [[[0.01,0.0],[0.0,0.01]] for i in range(6)]

    X = []
    C = []
    Y = []
    for i in xrange(1000):
        y = np.random.randint(0,2)
        c = np.random.choice([0,2,4] if y==1 else [1,3,5])
        x = np.random.multivariate_normal(mus[c], sigmas[c])

        X.append(x)
        C.append(c)
        Y.append(y)
    X = np.array(X)
    C = np.array(C, dtype=int)
    Y = np.array(Y, dtype=int)

    logZ = np.zeros((1000,2))
    logZ[Y==1] = np.array([0.0, 0.2])
    logZ[Y==0] = np.array([0.2, 0.0])

    #MODEL INITIALIZATION
    kmeans = KMeans(n_clusters=6).fit(X)

    model = ModelMultiGaussians(np.zeros((6,2)), np.zeros((6,2,2)))
    model.train(X, kmeans.labels_)

    switcher = Switcher(2, 6, 1000, np.ones(6)/6.0)

    var_z = np.random.randint(0, 2, size=1000)
    var_z[Y==0] = 1
    var_z[Y==1] = 0

    switcher.initN(var_z)

    print "START"
    print f1_score(Y, var_z, average="macro")
    #GIBBS SAMPLING
    for i in range(1000):
        switcher.sampleC(model.logL(X), var_z)
        var_z = switcher.sampleZ(var_z, logZ)

        print f1_score(Y, var_z, average="macro")
        print switcher.calcExpectedTheta()
