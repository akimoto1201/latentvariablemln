INCLUDES = -I/usr/include/python2.7
BOOSTFLAGS = -shared -fPIC
LIBS = -lboost_python -lpython2.7

mcsat_cpp.o: mcsat_cpp.cpp
	g++ mcsat_cpp.cpp -c -g $(INCLUDES) -fPIC -o mcsat_cpp.o

mcsat_cpp.so: mcsat_cpp.o
	g++ mcsat_cpp.o -g $(INCLUDES) $(BOOSTFLAGS) $(LIBS) -o mcsat_cpp.so
