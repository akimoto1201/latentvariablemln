# coding: UTF-8

import sys
from os import path

#sys.path.append('./python')
ROOT = path.dirname(path.abspath(__file__))
sys.path.append(ROOT + '/python')

from MLN import MarkovLogicNetwork
from MLN.inference import MCSAT, SampleSAT
from MLN.util import toCNF
import FOL

import numpy as np
from copy import copy

from base import MLN, MCSAT_Sampler

import mcsat_cpp
import time

class MCSAT_Sampler_CPP:
    def __init__(self, mln, simplify):
        u"""
        [Keyword arguments]
            mln -- MLNインスタンス
        """
        self.mrf = mln.mln.mrf
        self.mln = mln

        self.dataObject = mcsat_cpp.MCSAT_DataObject()

        #blockに関する情報
        _atomIdx2BlockIdx = [None] * len(self.mrf.gndAtoms)
        for atomIdx, blockIdx in self.mrf.atom2BlockIdx.items():
            _atomIdx2BlockIdx[atomIdx] = blockIdx
        _blockIdx2AtomIdx = [None] * len(self.mrf.pllBlocks)
        for blockIdx in xrange(len(self.mrf.pllBlocks)):
            idxGA, block = self.mrf.pllBlocks[blockIdx]
            if type(block) == type(None):
                _blockIdx2AtomIdx[blockIdx] = [idxGA]
            else:
                _blockIdx2AtomIdx[blockIdx] = block

        self.dataObject.initialize(_atomIdx2BlockIdx, _blockIdx2AtomIdx, mln.getNFormula())

        self.prev_formula_sign = None

        self.simplify = simplify

    def convertFormulasIntoClauses(self):
        u"""
        mlnに含まれるgndFormulaをCNFに変形し、clauseに分解する

        formulaの重みに依存する
        """
        formula_sign = tuple([1 if self.mln.getFormulaWeight(idx) >=0.0 else -1 for idx in xrange(self.mln.getNFormula())])
        if self.prev_formula_sign == formula_sign:
            print "use previous CNF"
            gndFormulas, formulas = self._bu_gndFormulas, self._bu_formulas

            formula_weights = {idx:abs(self.mln.getFormulaWeight(idx)) for idx in xrange(self.mln.getNFormula())}

            _gndFormula_weight = []
            for f in gndFormulas:
                _gndFormula_weight.append(formula_weights[f.idxFormula])
            self.dataObject.setClauseInformationOnlyWeight(_gndFormula_weight)
        else:
            print "parsing formulas into CNF: simplify=%s"%(str(self.simplify))
            self.triviallySatisfiedCount = [0] * self.mln.getNFormula()
            gndFormulas, formulas = toCNF(self.mrf.gndFormulas, self.mrf.formulas, allPositive=True, triviallySatisfiedCount = self.triviallySatisfiedCount, simplify=self.simplify, mrf=self.mln.mrf)
            self._bu_gndFormulas = gndFormulas
            self._bu_formulas = formulas

            #重みのmap
            formula_weights = {f.idxFormula : f.weight for f in formulas}

            _gndFormula_start = []#各gndFormulaに対応するclauseのidxの開始点
            _gndFormula_end = []#同上終了点（exclusive）
            _gndFormula_weight = []#各gndFormulaに対応する重み
            _formulaIdx = []
            _gndAtomIdxs = []#clauseに含まれるgndAtomIdxのリスト
            _gndAtomNegs = []#上のgndAtomIdxsにおける対応するgndAtomがnegatedされているかどうか(True or False) ※否定されているとTrueなのに注意
            _clause_start = []#あるclauseについて、上のgndAtomIdxsリストにおける開始点
            _clause_end = []#同上終了点(exclusive)
            _atom2clauseIdx = [[] for i in xrange(len(self.mrf.gndAtoms))]#atomIdx => list of clause_idx
            _atom2clauseNeg = [[] for i in xrange(len(self.mrf.gndAtoms))]#atomIdx => list of if_negated

            clause_counter = 0
            atom_counter = 0

            for f in gndFormulas:
                #clauseに分解
                if type(f) == FOL.Conjunction:
                    lc = f.children
                else:
                    lc = [f]
                #始点、終了点を登録
                _gndFormula_start.append(clause_counter)
                #重みを登録
                _gndFormula_weight.append(formula_weights[f.idxFormula])
                _formulaIdx.append(f.idxFormula)
                #clause内のliteralに分解
                for clause in lc:

                    if hasattr(clause, "children"):
                        lits = clause.children
                    else:
                        lits = [clause]

                    #各literalの開始位置、終了位置を登録
                    _clause_start.append(atom_counter)
                    #各リテラルについてground atomを登録
                    for lit in lits:
                        _gndAtomIdxs.append(lit.gndAtom.idx)
                        _gndAtomNegs.append(True if lit.negated else False)
                        _atom2clauseIdx[lit.gndAtom.idx].append(clause_counter)
                        _atom2clauseNeg[lit.gndAtom.idx].append(lit.negated)
                        atom_counter += 1
                    _clause_end.append(atom_counter)
                    clause_counter += 1
                _gndFormula_end.append(clause_counter)

            self.dataObject.setClauseInformation(
                _gndFormula_start,
                _gndFormula_end,
                _gndFormula_weight,
                _formulaIdx,
                _gndAtomIdxs,
                _gndAtomNegs,
                _clause_start,
                _clause_end,
                _atom2clauseIdx,
                _atom2clauseNeg
            )
        self.prev_formula_sign = formula_sign

    def setEvidence(self, evid):
        u"""
        evidenceを設定する
        """
        _evid = {self.mrf.gndAtoms[gndAtomStr].idx: True if tv else False for gndAtomStr, tv in evid.items()}

        self.dataObject.setEvidence(_evid)

    def sampleClauseSet(self, state):
        u"""
        現在真である論理式から1-exp(-w)の確率でサンプルした集合を返す
        """
        satisfied_count = mcsat_cpp.sampleClauseSet(state, self.dataObject)
        satisfied_count = np.array(satisfied_count) + np.array(self.triviallySatisfiedCount)
        for formulaIdx in range(self.mln.getNFormula()):
            if self.prev_formula_sign[formulaIdx] < 0:
                #flipしていたら真偽が逆になるから、全groundingの数から満たされている「ことになっている」数を引く
                satisfied_count[formulaIdx] = self.mln.getNumberOfFormulaGrounding(formulaIdx) - satisfied_count[formulaIdx]
        return satisfied_count

    def sampleSAT(self, state, p = 0.5, T = 0.1, maxIter=10000, verbose=False):
        state = mcsat_cpp.sampleSAT(state, self.dataObject, p, T, maxIter, verbose)
        return state

class MCSAT_Sampler2(object):
    def __init__(self, mln, simplify=True):
        u"""
        simplifyを用いると、evidenceを与えた時にもう一度CNF化し直す
        """
        assert isinstance(mln, MLN), "constructor of MCSAT_Sampler2 requires MLN instance!!! (NOT MarkovRandomField instance)"

        self.sampler = MCSAT_Sampler_CPP(mln, simplify)
        self.mln = mln

        self.simplify = simplify

    def initialize(self, state, mln, evidence, ifEvidenceChanged = False, ifWeightUpdated = False):
        u"""
        新しくMC-SATのchainを始めるときに実行する
        [Keyword arguments]
            state -- 初期state
            mln -- MLNインスタンス
            evidence -- hard evidence
            ifEvidenceChanged -- hard evidenceが前回から変化した場合に設定する
            ifWeightUpdated -- 重みが前回から変化した場合に設定する (もしくはこのsamplerを初めて使うとき)
        """
        if ifEvidenceChanged:
            self.mln.mrf.setEvidence(evidence, clear=True)
            if self.simplify:
                self.sampler.prev_formula_sign = None
                self.init()

        if ifWeightUpdated:
            self.init()

        if ifWeightUpdated or ifEvidenceChanged:
            mln.applyEvidence(evidence, state)
            self.sampler.setEvidence(evidence)

        return state

    def init(self):
        self.sampler.convertFormulasIntoClauses()

    def updateState(self, state, p = 0.5, T = 0.1, maxIter=10000, doSampleClauseSet=True, doSample=True, verbose=False):
        u"""
        MC-SATに従いstateを更新する

        ※initWithEvidenceを用いてもevidenceの変数が指定されるだけでstateは更新されないので、別途stateも更新する必要がある

        maxIterを指定するとMC-SATにおける最大試行回数を設定できる

        doSampleClauseSetを指定してdoSampleを指定しないと、充足される論理式のgroundの個数のリストを返す
        """
        #TODO: c++の実装で、np._bool用のretvalのconverterを作る
        state = [True if tv else False for tv in state]
        if doSampleClauseSet:
            satisfied_count = self.sampler.sampleClauseSet(state)
        if doSample:
            state = self.sampler.sampleSAT(state, p, T, maxIter, verbose)
        if doSample:
            return state
        elif doSampleClauseSet:
            return satisfied_count

if __name__=="__main__":
    import time
    N = 5
    inst = ",".join([str(x) for x in range(N)])
    PROG_STR = """
inst={%s}
val={%s}

A(inst, val)
B(inst, val)
C(inst, val)

1.0 A(i,v) v B(i,v) => C(i,v)
1.0 A(i,v) ^ B(i2,0) => C(i,0) ^ C(i2,0)
"""%(inst, inst)
    mln = MLN(prog_str=PROG_STR)

    for i in xrange(mln.getNGndAtom()):
        pass
        #print i, mln.getGndAtom(i)



    state = [False for i in range(mln.getNGndAtom())]
    evid = {"A(%d,%d)"%(i,j):np.random.uniform()<0.5 for i in range(N) for j in range(N)}
    mln.applyEvidence(evid, state)

    init_state = copy(state)

    for atomStr, tv in evid.items():
        atomIdx = mln.getGndAtomIdxByStr(atomStr)
        if state[atomIdx] ^ tv:
            print "illegal state1!!!!!"
            input()

    mcsat = MCSAT_Sampler_RAW(mln)
    _mcsat_cpp = MCSAT_Sampler_CPP(mln)
    mcsat.convertFormulasIntoClauses()
    _mcsat_cpp.convertFormulasIntoClauses()
    mcsat.setEvidence(evid)
    _mcsat_cpp.setEvidence(evid)

    mcsat.sampleClauseSet(state)
    _mcsat_cpp.sampleClauseSet(state)
    #print state
    #state = _mcsat_cpp.sampleSAT(state,T=14.0)
    #print state
    #input('input')

    # for i in xrange(mln.getNGndAtom()):
    #     print i, mln.getGndAtom(i),state[i]

    for atomStr, tv in evid.items():
        atomIdx = mln.getGndAtomIdxByStr(atomStr)
        if state[atomIdx] ^ tv:
            print "illegal state2!!!!!"
            input()

    start = time.clock()
    chain = []
    for i in range(10000):
        sys.stdout.write('%d\r'%i)
        sys.stdout.flush()

        _mcsat_cpp.sampleClauseSet(state)
        state = _mcsat_cpp.sampleSAT(state, T=14.0)
        chain.append(copy(state))
    print "time",time.clock() - start

    for atomStr, tv in evid.items():
        atomIdx = mln.getGndAtomIdxByStr(atomStr)
        if state[atomIdx] ^ tv:
            print "illegal state3!!!!!"
            input()

    #print M

    #以下relsatを用いたSampleSATの検証
    # import cPickle as pic
    # if True:
    #     pic.dump(M, open("_test_M.tmp","w"))
    #
    #     h = open('_test_relsat.data',"w")
    #     h.write('p cnf %d %d\n'%(mln.getNGndAtom(), len(M)+6))
    #     for clause in M:
    #         for gndAtomIdxs, ifNegated in zip(mcsat.gndAtomIdxs, mcsat.gndAtomNegs)[mcsat.clause_start[clause]:mcsat.clause_end[clause]]:
    #             h.write(('-' if ifNegated else '')+'%d'%(gndAtomIdxs+1)+' ')
    #         h.write('0\n')
    #     for inst in [0,1]:
    #         A = mln.getGndAtomIdxByStr('A(%d,0)'%inst) + 1
    #         B = mln.getGndAtomIdxByStr('A(%d,1)'%inst) + 1
    #         h.write('-%d -%d 0 %d %d 0\n'%(A,B,A,B))
    #     h.write('%d 0\n'%(mln.getGndAtomIdxByStr('A(0,0)')+1))
    #     h.write('%d 0\n'%(mln.getGndAtomIdxByStr('A(1,1)')+1))
    #     h.close()
    # else:
    #     M = pic.load(open("_test_M.tmp","r"))
    #
    # ans_hash = {}
    # for i in xrange(100000):
    #     sys.stdout.write('%d\r'%i)
    #     sys.stdout.flush()
    #
    #     state = mcsat.sampleSAT(state, M, T=14.0)
    #
    #     tup = tuple([int(x) for x in state])
    #     if ans_hash.has_key(tup):
    #         ans_hash[tup] += 1
    #     else:
    #         ans_hash[tup] = 1
    #
    # h = open("_test_mcsat.log","w")
    # for key, val in ans_hash.items():
    #     print >> h, key, "-", val
    # h.close()

    h = open("_test_mcsat.txt","w")

    chain = np.array(chain)
    p_cpps = [np.mean(chain[i:], axis=0) for i in [-10,-30,-100,-300,-1000,-3000,-10000]]

    # start = time.clock()
    # chain = []
    # state = copy(init_state)
    # for i in range(1000):
    #     sys.stdout.write('%d\r'%i)
    #     sys.stdout.flush()
    #
    #     M = mcsat.sampleClauseSet(state)
    #     print "M",len(M)
    #     state = mcsat.sampleSAT(state,M,  T=14.0)
    #     chain.append(copy(state))
    # print "time",time.clock() - start
    #
    # chain = np.array(chain)
    # p_python = np.mean(chain, axis=0)

    chain = []
    state = copy(init_state)
    sampler = MCSAT_Sampler(mln.mln.mrf)
    sampler.initialize(state, mln, evid, ifEvidenceChanged=True, ifWeightUpdated=True)
    for i in range(10000):
        sys.stdout.write('%d\r'%i)
        sys.stdout.flush()
        state = sampler.updateState(state, maxIter=10000)
        chain.append(copy(state))
    chain = np.array(chain)
    ps = [np.mean(chain[i:], axis=0) for i in [-10,-30,-100,-300,-1000,-3000,-10000]]

    for atomStr in ['%s(%d,%d)'%(pred,i,j) for pred in ['A', 'B', 'C'] for i in range(N) for j in range(N)]:
        print >> h, atomStr, np.array_str(np.array([p[mln.getGndAtomIdxByStr(atomStr)] for p in ps]), precision=3), np.array_str(np.array([p_cpp[mln.getGndAtomIdxByStr(atomStr)] for p_cpp in p_cpps]), precision=3)


    # gndFormulas, formulas = toCNF(mln.mrf.gndFormulas, mln.mrf.formulas, allPositive=True)
    #
    # print "gndFormula"
    # for gndFormula in gndFormulas:
    #     print "------"*5
    #     print gndFormula
    #     f = gndFormula
    #     if type(gndFormula) == FOL.Conjunction:
    #         lc = f.children
    #     else:
    #         lc = [f]
    #     for c in lc:
    #         if hasattr(c, 'children'):
    #             lits = c.children
    #         else:
    #             lits = [c]
    #         for lit in lits:
    #             print '-----'
    #             print lit.gndAtom.idx
    #             print lit.negated
    # print "="*60
    # print "formulas"
    # for formula in formulas:
    #     print '---'
    #     print formula
    #     formula_cnf = formula.toCNF()
    #     print formula_cnf
    #
    #     print formula_cnf.getVariables(mln.mln)
