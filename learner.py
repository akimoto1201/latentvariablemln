#coding: UTF-8

import sys
from copy import copy

import numpy as np
from scipy.optimize import fmin_l_bfgs_b, fmin_bfgs
from scipy.misc import logsumexp

from util import Adam

from mcsat import MCSAT_Sampler2
from base import MLN

class MCSAT_DN_Learner(object):
    u"""
    [D Lowd and P Domingos, 2007]Efficient weight learning for Markov logic Networks
    において提案されているDiagonal Newton法による学習アルゴリズム
    """
    def __init__(self, mln, verbose=False, mcsat_verbose=False, simplify=True):
        self._mln = mln
        self._sampler = MCSAT_Sampler2(mln, simplify=simplify)
        self._verbose = verbose
        self._mcsat_verbose = mcsat_verbose

    def _sample(self, init_state, evid, wt, T, maxIter, sample_step=5):
        u"""
        現在の重みにおける各論理式の充足されるgroundingの個数をMC-SATによりサンプルする

        [Keyword arguments]
            init_state -- 初期状態となるground atomへの真理値の割り当て。 boolのlist
            evid -- 状態が満たすべきevidence。通常のatomStrをキーとし、truth valueを値とするような辞書。
            wt -- 現在の重み
            sample_step -- サンプルする回数。（前回の状態を再利用する前提なので、burn-outの期間は設けない）
        [Return]
            n -- (n_step, n_formula)の次元を持つndarray。 各ステップにおいて各論理式が充足されている個数を示す。np.float32
        """
        #念の為重みを初期化
        self._mln.setFormulaWeights(wt)
        #戻り値の初期化
        n = []
        #サンプル
        state = copy(init_state)
        self._sampler.initialize(state, self._mln, evid, ifEvidenceChanged=False, ifWeightUpdated=True)
        self._sampler.updateState(state, doSample=False)
        for step in xrange(sample_step):
            if self._verbose:
                sys.stdout.write("step: %d - %d\r"%(step+1,sample_step))
                sys.stdout.flush()
            state = self._sampler.updateState(state, doSampleClauseSet=False, verbose=self._mcsat_verbose, T=T, maxIter=maxIter)
            if self._mcsat_verbose:
                sys.stdout.write('\n')
                sys.stdout.flush()
            satisfied_count = np.array(self._sampler.updateState(state, doSample=False))
            if self._mcsat_verbose:
                print "satisfied count: ",satisfied_count
            n.append(satisfied_count)
        #充足個数を返す
        n = np.array(n, dtype=np.float32)
        return n,state

    def learnWeight(self, chain, evid, sample_step=20, lam=1.0, conv_threshold=1e-4, maxEpoch=1000, T=14.0, maxIter=10000, learnFormula=None, lambdaFormula=None, muFormula=None):
        u"""
        重みを学習する

        [Keyword arguments]
            chain -- 訓練データとなる状態列（現状最後の状態しか用いていない）
            evid -- 状態が満たすべきevidence
            sample_step -- MC-SATに用いるサンプル数
            lam -- diagonal newtonのステップサイズ調整に用いる初期値
            conv_threshold -- ||w_t1 - w_t|| < threshold (1+||xt1||)となったとき収束判定する
            maxEpoch -- 学習の最大イタレーション回数
            T, maxIter -- MC-SATに用いるパラメータ
            learnFormula -- 学習する論理式のidxのリスト
            lambdaFormula -- 各重みのGaussian事前分布の分散の逆数
            muFormula -- 各重みのGaussian事前分布の平均
        """
        if type(learnFormula) == type(None):
            learnFormula = range(self._mln.getNFormula())
        if type(lambdaFormula) == type(None):
            lambdaFormula = [0.0]*len(learnFormula)
        if type(muFormula) == type(None):
            muFormula = [0.0]*len(learnFormula)
        lambdaFormula = np.array(lambdaFormula)
        muFormula = np.array(muFormula)
        assert len(learnFormula) == lambdaFormula.shape[0]
        assert lambdaFormula.shape[0] == muFormula.shape[0]

        #重みを初期化
        wt = np.array([self._mln.getFormulaWeight(idxFormula) for idxFormula in xrange(self._mln.getNFormula())])
        #訓練データにおうて充足されている個数を計算
        self._sampler.initialize(chain[-1], self._mln, evid, ifEvidenceChanged=True, ifWeightUpdated=True)
        n_true = np.array(self._sampler.updateState(chain[-1], doSample=False))[learnFormula]

        def calc_gh(_state, _wt, old_ns=None):
            if self._verbose:
                print _wt
            #充足個数をサンプル
            ns, _state = self._sample(_state, evid, _wt, T, maxIter, sample_step=sample_step)
            ns = ns[:,learnFormula]
            if type(old_ns) != type(None):
                ns = np.concatenate((old_ns, ns), axis=0)
            #print ns
            #negative log-likelihoodの勾配、へっシアンの対角要素を計算
            n_ave = np.mean(ns, axis=0)
            n2_ave = np.mean(ns*ns, axis=0)
            g = - n_true + n_ave
            print "weight", _wt
            print "n_true", n_true
            print "n_ave", n_ave
            #二乗から二乗を引く方式だと数値誤差でマイナスになってしまったりしたので、分散を通して計算する方式に変更
            #h = n2_ave - n_ave*n_ave
            h = np.var(ns, axis=0)
            #Gaussian事前分布の寄与を加える
            g += (_wt[learnFormula] - muFormula) * lambdaFormula
            raw_h = np.copy(h)
            h += lambdaFormula
            assert np.min(h) > 0.0, "some of Hessian component is zero!!: %s\t %s"%(np.array_str(raw_h),np.array_str(h))
            return g,h,ns,_state

        #学習
        state = chain[-1]
        #最初のステップ用の勾配とヘッシアン対角要素を計算しておく
        try:
            g_t, h_t, ns_t, state_t = calc_gh(state, wt)
            n_iter = 0
            while n_iter < maxEpoch:
                #収束判定
                flag_converged = n_iter > 1 and np.linalg.norm(wt-wt_tm1) < conv_threshold * (1.0 + np.linalg.norm(wt))
                if flag_converged:
                    if self._verbose:
                        print "successfully converged"
                    break
                else:
                    if self._verbose:
                        print "Iteration::%d"%n_iter
                if np.min(np.absolute(g_t)) < 1e-5:
                    if self._verbose:
                        print "gradient is zero. converged."
                    break
                #現在の勾配とヘッシアンに従い更新してみる
                #alphaの計算
                #g_var = np.var(ns_t, axis=0)
                #NOTE:Alchemyにおいてはここでd=g/varianceとしていたが、これはつまるところd=g/hと等価であった
                d = np.array([0.0 if np.absolute(g) < 1e-8 else - g / h for g,h in zip(g_t, h_t)])
                dns = ns_t * d[np.newaxis,:]
                dHd = np.mean(np.power(np.sum(dns,axis=1),2)) - np.mean(np.sum(dns,axis=1)) ** 2 + np.sum(d * d * lambdaFormula)
                assert dHd >= 0.0, "%f %f %f %f"%(dHd, np.mean(np.power(np.sum(dns,axis=1),2)), np.mean(np.sum(dns,axis=1)) ** 2, np.sum(d * d * lambdaFormula))
                alpha = - np.dot(d, g_t) / (dHd + lam * np.dot(d, d))
                d_t = - alpha * g_t / h_t
                #重みの更新
                if self._verbose:
                    print "n_sample: %d"%ns_t.shape[0]
                    print "gradient: %s"%np.array_str(g_t, precision=5)
                    #print "variance: %s"%np.array_str(g_var, precision=5)
                    print "diagnoal Hessian: %s"%np.array_str(h_t, precision=5)
                    print "d: ",np.array_str(d, precision=5)
                    print "d*g_t: ",np.dot(d, g_t)
                    print "dHd: ", dHd
                    print "update step: %s"%np.array_str(d_t, precision=5)
                    print "lam: %f"%lam
                    print "alpha: %f"%alpha
                    print "current weight: %s"%np.array_str(wt, precision=5)
                new_wt = np.copy(wt)
                new_wt[learnFormula] += d_t
                assert np.sum(np.isnan(new_wt)) < 1, "NaN detected!! %s"%(np.array_str(new_wt),)
                if self._verbose:
                    print "new weight: %s"%np.array_str(new_wt, precision=5)
                #
                new_g, new_h, new_ns, new_state = calc_gh(state_t, new_wt)
                #現在の点における更新の結果を評価する
                #ここでのd_tは探索方向ではなく、実際の更新ステップ
                dns = ns_t * d_t[np.newaxis,:]
                #NOTE: 現論文においてここがdHgとなっていたのはおそらく誤植か
                gns = ns_t * d_t[np.newaxis,:]
                dtHgt = np.mean(np.sum(dns,axis=1)*np.sum(gns, axis=1)) - np.mean(np.sum(dns,axis=1)) * np.mean(np.sum(gns, axis=1)) + np.dot(d_t * lambdaFormula, g_t)
                delta_pred = np.dot(d_t, g_t) + 0.5 * dtHgt
                delta_act = np.dot(d_t, new_g)
                frac = delta_act / delta_pred
                if frac > 0.75:
                    lam *= 0.5
                elif frac < 0.25:
                    lam *= 4
                if self._verbose:
                    print "d_t * g_t: %f dt*H*gt: %f"%(np.dot(d_t, g_t), dtHgt)
                    print "predicted delta: %f upper bound of actual delta: %f frac: %f"%(delta_pred, delta_act, frac)
                #delta_actが負だった場合のみ新しい値を採用する (ただし計算誤差も含めて若干の幅は持たせる)
                if delta_act < 1e-8:
                    g_t, h_t, ns_t, state_t = new_g, new_h, new_ns, new_state
                    wt_tm1 = np.copy(wt)
                    wt = new_wt
                    n_iter += 1
                else:
                    g_t, h_t, ns_t, state_t = calc_gh(state, wt, old_ns=ns_t)
                    if self._verbose:
                        print "Backtracking!!"
        except AssertionError as e:
            print "detect assertion error!!: %s"%(str(e))
        self._mln.setFormulaWeights(wt)

class MCSAT_L1_Learner(object):
    u"""
    [D Lowd and P Domingos, 2007]Efficient weight learning for Markov logic Networks
    において提案されているDiagonal Newton法による学習アルゴリズム
    """
    def __init__(self, mln, verbose=False, mcsat_verbose=False):
        self._mln = mln
        self._sampler = MCSAT_Sampler2(mln)
        self._verbose = verbose
        self._mcsat_verbose = mcsat_verbose

    def _sample(self, init_state, evid, wt, T, maxIter, sample_step=5):
        u"""
        現在の重みにおける各論理式の充足されるgroundingの個数をMC-SATによりサンプルする

        [Keyword arguments]
            init_state -- 初期状態となるground atomへの真理値の割り当て。 boolのlist
            evid -- 状態が満たすべきevidence。通常のatomStrをキーとし、truth valueを値とするような辞書。
            wt -- 現在の重み
            sample_step -- サンプルする回数。（前回の状態を再利用する前提なので、burn-outの期間は設けない）
        [Return]
            n -- (n_step, n_formula)の次元を持つndarray。 各ステップにおいて各論理式が充足されている個数を示す。np.float32
        """
        #念の為重みを初期化
        self._mln.setFormulaWeights(wt)
        #戻り値の初期化
        n = []
        #サンプル
        state = copy(init_state)
        self._sampler.initialize(state, self._mln, evid, ifEvidenceChanged=False, ifWeightUpdated=True)
        self._sampler.updateState(state, doSample=False)
        for step in xrange(sample_step):
            if self._verbose:
                sys.stdout.write("step: %d - %d\r"%(step+1,sample_step))
                sys.stdout.flush()
            state = self._sampler.updateState(state, doSampleClauseSet=False, verbose=self._mcsat_verbose, T=T, maxIter=maxIter)
            if self._mcsat_verbose:
                sys.stdout.write('\n')
                sys.stdout.flush()
            satisfied_count = np.array(self._sampler.updateState(state, doSample=False))
            if self._mcsat_verbose:
                print satisfied_count
            n.append(satisfied_count)
        #充足個数を返す
        n = np.array(n, dtype=np.float32)
        return n,state

    def learnWeight(self, chain, evid, sample_step=20, beta = 0.1, conv_threshold=1e-4, maxEpoch=1000, T=14.0, maxIter=10000, learnFormula=None, lambdaFormula=None, muFormula=None, L1Formula=None):
        u"""
        重みを学習する

        [Keyword arguments]
            chain -- 訓練データとなる状態列（現状最後の状態しか用いていない）
            evid -- 状態が満たすべきevidence
            sample_step -- MC-SATに用いるサンプル数
            beta -- 近接勾配法におけるbacktracking時に用いる縮小率
            conv_threshold -- ||w_t1 - w_t|| < threshold (1+||xt1||)となったとき収束判定する
            maxEpoch -- 学習の最大イタレーション回数
            T, maxIter -- MC-SATに用いるパラメータ
            learnFormula -- 学習する論理式のidxのリスト
            lambdaFormula -- 各重みのGaussian事前分布の分散の逆数
            muFormula -- 各重みのGaussian事前分布の平均
            L1Formula -- L1正則化係数
        """
        if type(learnFormula) == type(None):
            learnFormula = range(self._mln.getNFormula())
        if type(lambdaFormula) == type(None):
            lambdaFormula = [0.0]*len(learnFormula)
        if type(muFormula) == type(None):
            muFormula = [0.0]*len(learnFormula)
        if type(L1Formula) == type(None):
            L1Formula = [0.0]*len(learnFormula)
        lambdaFormula = np.array(lambdaFormula)
        muFormula = np.array(muFormula)
        assert len(learnFormula) == lambdaFormula.shape[0]
        assert lambdaFormula.shape[0] == muFormula.shape[0]

        #重みを初期化
        wt = np.array([self._mln.getFormulaWeight(idxFormula) for idxFormula in xrange(self._mln.getNFormula())])
        #訓練データにおうて充足されている個数を計算
        self._sampler.initialize(chain[-1], self._mln, evid, ifEvidenceChanged=True, ifWeightUpdated=True)
        n_true = np.array(self._sampler.updateState(chain[-1], doSample=False))[learnFormula]

        def calc_gh(_state, _wt, old_ns=None):
            print _wt
            #充足個数をサンプル
            ns, _state = self._sample(_state, evid, _wt, T, maxIter, sample_step=sample_step)
            ns = ns[:,learnFormula]
            if type(old_ns) != type(None):
                ns = np.concatenate((old_ns, ns), axis=0)
            #print ns
            #negative log-likelihoodの勾配、へっシアンの対角要素を計算
            n_ave = np.mean(ns, axis=0)
            n2_ave = np.mean(ns*ns, axis=0)
            g = - n_true + n_ave
            #二乗から二乗を引く方式だと数値誤差でマイナスになってしまったりしたので、分散を通して計算する方式に変更
            #h = n2_ave - n_ave*n_ave
            h = np.var(ns, axis=0)
            #Gaussian事前分布の寄与を加える
            g += (_wt[learnFormula] - muFormula) * lambdaFormula
            raw_h = np.copy(h)
            h += lambdaFormula
            assert np.min(h) > 0.0, "some of Hessian component is zero!!: %s\t %s"%(np.array_str(raw_h),np.array_str(h))
            return g,h,ns,_state

        #学習
        state = chain[-1]
        #最初のステップ用の勾配とヘッシアン対角要素を計算しておく
        try:
            g_t, h_t, ns_t, state_t = calc_gh(state, wt)
            n_iter = 0
            #近接勾配法の更新ステップを、現在のヘッシアンによるLipschitz性から推定
            L = np.max(h_t)
            step_t = 1.0 / L
            while n_iter < maxEpoch:
                #収束判定
                flag_converged = n_iter > 1 and np.linalg.norm(wt-wt_tm1) < conv_threshold * (1.0 + np.linalg.norm(wt))
                if flag_converged:
                    if self._verbose:
                        print "successfully converged"
                    break
                else:
                    if self._verbose:
                        print "Iteration::%d"%n_iter
                if np.min(np.absolute(g_t)) < 1e-5:
                    if self._verbose:
                        print "gradient is zero. converged."
                    break
                #近接勾配法で更新
                #現在の重みで計算
                w_cand = wt - step_t * g_t
                #prox作用素を適用
                for i_lF, lF in enumerate(learnFormula):
                    _lam = L1Formula[i_lF]
                    if abs(w_cand[lF]) < _lam:
                        w_cand[lF] = 0.0
                    elif w_cand[lF] > _lam:
                        w_cand[lF] -= _lam
                    else:
                        w_cand[lF] += _lam
                if self._verbose:
                    print "n_sample: %d"%ns_t.shape[0]
                    print "gradient: %s"%np.array_str(g_t, precision=5)
                    print "diagnoal Hessian: %s"%np.array_str(h_t, precision=5)
                    print "step_t: %f"%step_t
                    print "current weight: %s"%np.array_str(wt, precision=5)
                    print "new weight: %s"%np.array_str(w_cand, precision=5)
                assert np.sum(np.isnan(w_cand)) < 1, "NaN detected!! %s"%(np.array_str(w_cand),)

                #新しい重みでサンプル
                new_g, new_h, new_ns, new_state = calc_gh(state_t, w_cand)
                #現在の点における更新の結果を評価する
                #ここでのd_tは探索方向ではなく、実際の更新ステップ
                d_t = w_cand - wt
                delta_pred = - step_t * np.dot(g_t, d_t) + 0.5 * step_t * np.dot(d_t, d_t)
                delta_act = np.dot(d_t, new_g)#実際の変化の上限(つまりこれよりは下がっている)

                if self._verbose:
                    print "update step: %s"%np.array_str(d_t, precision=5)
                    print "gradient at updated point: %s"%np.array_str(new_g, precision=5)
                    print "diaonal hessian at updated point: %s"%np.array_str(new_h, precision=5)

                if abs(delta_pred) < 1e-8:
                    print "small delta_pred: %f. converged?"%delta_pred
                    break

                #厳密にはfracは1.0以上でないといけない
                if self._verbose:
                    print "predicted delta: %f upper bound of actual delta: %f"%(delta_pred, delta_act)
                #delta_actが負だった場合のみ新しい値を採用する (ただし計算誤差も含めて若干の幅は持たせる)
                if delta_act < delta_pred and delta_act < 1e-8:
                    g_t, h_t, ns_t, state_t = new_g, new_h, new_ns, new_state
                    wt_tm1 = np.copy(wt)
                    wt = w_cand
                    n_iter += 1
                    #次の重みを用いてステップの初期値も再計算
                    L = np.max(h_t)
                    step_t = 1.0 / L
                else:
                    g_t, h_t, ns_t, state_t = calc_gh(state, wt, old_ns=ns_t)
                    #ステップを縮める
                    step_t *= beta
                    if self._verbose:
                        print "Backtracking!!"
        except AssertionError as e:
            print "detect assertion error!!: %s"%(str(e))
        self._mln.setFormulaWeights(wt)

if __name__=="__main__":
    from alchemy import learnMLNviaAlchemy

    N = 100

    PROG_STR = """
inst={%s}

Friend(inst,inst)
Smokes(inst)
Cancer(inst)

-0.5 Friend(i1,i2) ^ Smokes(i1) => Smokes(i2)
-0.5 Friend(i1,i2) ^ Smokes(i2) => Smokes(i1)
0.0 Smokes(i) => Cancer(i)
"""%(",".join([str(x) for x in range(N)]))

    mln = MLN(prog_str=PROG_STR)

    print "generating true evidence"
    friend = np.zeros((N,N), dtype=bool)
    smokes = np.zeros((N,), dtype=bool)
    cancer = np.zeros((N,), dtype=bool)
    for n in range(N):
        smokes[n] = True if np.random.uniform() < 0.5 else False
        cancer[n] = True if (np.random.uniform() < 0.2 and smokes[n]) or (np.random.uniform() < 0.8 and (not smokes[n])) else False
        for nn in range(N):
            if nn > n:
                friend[nn,n] = friend[n,nn] = True if np.random.uniform() < 0.5 else False
            elif nn == n:
                friend[nn,n] = True

    evid = {}
    evid.update({'Smokes(%d)'%n:smokes[n] for n in range(N)})
    evid.update({'Cancer(%d)'%n:cancer[n] for n in range(N)})
    evid.update({'Friend(%d,%d)'%(n,nn):friend[n,nn] for n in range(N) for nn in range(N)})
    #count number of trivially satisfied formula
    # count = 0
    # for i1 in xrange(N):
    #     for i2 in xrange(N):
    #         if not friend[i1,i2]:
    #             count += 1
    # print 'trivially satisfied count: ',count
    state = [False]*mln.getNGndAtom()
    #stateに正しい真理値を登録
    mln.applyEvidence(evid, state)

    hard_evid = {}
    hard_evid.update({'Friend(%d,%d)'%(n,nn):friend[n,nn] for n in range(N) for nn in range(N)})

    print "start learning"

    alchemyWeight = learnMLNviaAlchemy(mln, ["Cancer", "Smokes"], copy(state))
    #print alchemyWeight
    #input("proceed?")

    mln.setFormulaWeights([0.0,0.0,0.0])

    # learner = MCSAT_CD_Learner(mln)
    # learner.learnWeight(np.array([state]), hard_evid, k=50, mcsat_verbose=False, maxIter=100000, maxEpoch=100)

    learner = MCSAT_DN_Learner(mln, verbose=True, mcsat_verbose=True)
    learner.learnWeight(np.array([state]), hard_evid, sample_step=20, conv_threshold=1e-4, lambdaFormula=[0.0,0.0,0.0], muFormula=[0.0,0.0,0.0])
    print "Alchemy:",alchemyWeight

    input('proceed?')

    PROG_STR = """
inst={%s}

Friend(inst,inst)
Smokes(inst)
Cancer(inst)

-0.5 Friend(i1,i2) ^ Smokes(i1) => Smokes(i2)
-0.5 Friend(i1,i2) ^ Smokes(i2) => Smokes(i1)
0.0 Smokes(i) => Cancer(i)
0.0 Friend(i1,i2) ^ Cancer(i1) => Cancer(i2)
"""%(",".join([str(x) for x in range(N)]))

    mln = MLN(prog_str=PROG_STR)

    evid = {}
    evid.update({'Smokes(%d)'%n:smokes[n] for n in range(N)})
    evid.update({'Cancer(%d)'%n:cancer[n] for n in range(N)})
    evid.update({'Friend(%d,%d)'%(n,nn):friend[n,nn] for n in range(N) for nn in range(N)})

    state = [False]*mln.getNGndAtom()
    #stateに正しい真理値を登録
    mln.applyEvidence(evid, state)

    hard_evid = {}
    hard_evid.update({'Friend(%d,%d)'%(n,nn):friend[n,nn] for n in range(N) for nn in range(N)})

    print "start learning"


    mln.setFormulaWeights([0.0,0.0,0.0,0.0])

    # learner = MCSAT_CD_Learner(mln)
    # learner.learnWeight(np.array([state]), hard_evid, k=50, mcsat_verbose=False, maxIter=100000, maxEpoch=100)

    learner = MCSAT_DN_Learner(mln, verbose=True, mcsat_verbose=False)
    learner.learnWeight(np.array([state]), hard_evid, sample_step=20, conv_threshold=1e-4, learnFormula=[0,1,2])

    input('proceed?')

    state = [False]*mln.getNGndAtom()
    #stateに正しい真理値を登録
    mln.applyEvidence(evid, state)

    hard_evid = {}
    hard_evid.update({'Friend(%d,%d)'%(n,nn):friend[n,nn] for n in range(N) for nn in range(N)})

    print "start learning"


    mln.setFormulaWeights([0.0,0.0,0.0,0.0])

    # learner = MCSAT_CD_Learner(mln)
    # learner.learnWeight(np.array([state]), hard_evid, k=50, mcsat_verbose=False, maxIter=100000, maxEpoch=100)

    learner = MCSAT_DN_Learner(mln, verbose=True, mcsat_verbose=False)
    learner.learnWeight(np.array([state]), hard_evid, sample_step=20, conv_threshold=1e-4, learnFormula=[0,1,2,3])
