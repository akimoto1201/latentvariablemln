#coding: UTF-8

import sys
from os import path
FILE_DIR = path.dirname(path.abspath(__file__))
print FILE_DIR
ROOT = path.dirname(FILE_DIR)
print ROOT
sys.path.append(ROOT)

from copy import copy

import numpy as np

from base import MLN
from learner import MCSAT_L1_Learner, MCSAT_DN_Learner

if __name__=="__main__":
    N = 1000

    A = np.random.uniform(size=N) < 0.5
    B = np.random.uniform(size=N) < 0.5

    Ct = np.logical_and(np.logical_not(A), B)
    Cn = np.random.uniform(size=N) < 0.5
    C = np.where(np.random.uniform(size=N)<0.8, Ct, Cn)

    PROG_STR = """
inst={%s}

A(inst)
B(inst)
C(inst)

0.0 A(i) ^ B(i) <=> C(i)
0.0 !A(i) ^ B(i) <=> C(i)
0.0 A(i) ^ !B(i) <=> C(i)
0.0 !A(i) ^ !B(i) <=> C(i)
"""%(",".join([str(x) for x in range(N)]))

    mln = MLN(prog_str=PROG_STR)

    state = [np.random.uniform()<0.5 for i in xrange(mln.getNGndAtom())]
    evid = {}
    evid.update({"A(%d)"%(i,): A[i] for i in xrange(N)})
    evid.update({"B(%d)"%(i,): B[i] for i in xrange(N)})
    evid.update({"C(%d)"%(i,): C[i] for i in xrange(N)})
    mln.applyEvidence(evid, state)

    #L1を使った結果
    learner = MCSAT_L1_Learner(mln, verbose=True, mcsat_verbose=False)
    learner.learnWeight(np.array([state], dtype=bool), evid={}, sample_step=100, maxIter=100000, lambdaFormula=[0.1]*mln.getNFormula(), L1Formula=[0.3]*mln.getNFormula())
    l1_result = [mln.getFormulaWeight(i) for i in xrange(mln.getNFormula())]
    print l1_result
    input('proceed?')

    mln.setFormulaWeights(np.array([0.0]*mln.getNFormula()))
    #デフォルトの学習器
    learner = MCSAT_DN_Learner(mln, verbose=True, mcsat_verbose=True)
    learner.learnWeight(np.array([state], dtype=bool), evid={}, sample_step=100, maxIter=100000, lambdaFormula=[0.1]*mln.getNFormula())
    default_result = [mln.getFormulaWeight(i) for i in xrange(mln.getNFormula())]

    print "l1"
    print l1_result
    print "default"
    print default_result
