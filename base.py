# coding: UTF-8

u"""
基本的なクラスの実装

Predicateは、基本的にatomの管理と、Yのサンプルを統一的なインターフェースにより与える
Classifierは、SoftなYの推論に用いられ、ModelとSoftPredicateにより管理される

[現状の実装問題]
Modelクラスにおいて、predicateがすべてsoftである前提で実装されている（ここを変えるとどこに問題が現れるかも不明・・・）
できればHardPredicateも混ぜられるようにしたいが・・・
このような場合(Modelに含むような場合)は、パラメータを学習できない、仮想的なHardPredicateを用いることにするほうが実装の都合上無難か...
"""

import sys
from os import path

#sys.path.append('./python')
ROOT = path.dirname(path.abspath(__file__))
sys.path.append(ROOT + '/python')

from MLN import MarkovLogicNetwork
from MLN.inference import MCSAT, SampleSAT
from MLN.util import toCNF
import FOL

from itertools import product
import copy
import random
import math

import numpy as np

from util import Adam

class MLN(object):
    def __init__(self, prog_str, formula_groundings = None, predicate_groundings = None):
        u"""
        prog_strはAlchemy文法で書かれたmln文字列
        ※ただしinst={0,...,100}のような略記法は対応していない

        formula_groundings -- formulaIdxをキーとし、assignmentのリストを値とする辞書
        assignmentとは変数名をキーとし、変数の取る定数値を値に持つ辞書
        ※これによりgroundingを制限する場合、除くgroundingはevidenceにより真理値が自明となるもののみにし、learnerに対しそのevidenceをしっかり与えること
        evidence以外の述語に寄って真理値が変わってしまうようなgroundingは除かないこと

        predicate_groundings -- predicateの名前をキーとし、引数のリストのリストを値に持つ辞書
        """
        self.prog_str = prog_str

        self.mln = MarkovLogicNetwork.MLN(mlnContent = prog_str)

        #create empty database
        db = MarkovLogicNetwork.Database(self.mln)
        if type(formula_groundings) == type(None):
            formula_groundings = {}
        if type(predicate_groundings) == type(None):
            predicate_groundings = {}
        db.setFormulaGroundingInformation(formula_groundings, predicate_groundings)
        self.formula_groundings = formula_groundings

        #ground Markov random field
        print 'ground Markov random field'
        self.mrf = self.mln.groundMRF(db, method='UserSpecifiedGroundingFactory')
        self.mrf._getPllBlocks()
        self.mrf._getAtom2BlockIdx()

        #gndAtomIdx -> gndFormulaIdx
        #gndFormulaStr -> gndFormulaIdx
        n_gndAtom = len(self.mrf.gndAtoms)
        print 'number of ground Atom: %d'%n_gndAtom
        print 'creating gndAtom2gndFormulas'
        self._gndAtom2gndFormulas = [[] for i in xrange(n_gndAtom)]
        print 'creating gndAtom2gndFormulas'
        self._gndFormulaStr2Index = {}
        for i,gndFormula in enumerate(self.mrf.gndFormulas):
            self._gndFormulaStr2Index[str(gndFormula)] = i
            for gndAtom in gndFormula.idxGroundAtoms():
                self._gndAtom2gndFormulas[gndAtom].append(i)

        #formulaIdx => groundFormulaNum
        self._n_ofFormulaGrounding = [len(self.formula2GndFormulas(formulaIdx)) for formulaIdx in range(self.getNFormula())]

    def getNGndAtom(self):
        return len(self.mrf.gndAtoms)

    def getNFormula(self):
        return len(self.mrf.formulas)

    def getNGndFormula(self):
        return len(self.mrf.gndFormulas)

    def getNGndBlock(self):
        return len(self.mrf.pllBlocks)

    def getGndAtom(self, idx):
        return self.mrf.gndAtomsByIdx[idx]

    def getGndAtomByStr(self, name):
        u"""
        [Return]
            対応するground atomのGroundAtomインスタンス
        """
        return self.mrf.gndAtoms[name]

    def getGndAtomIdxByStr(self, name):
        u"""
        [Return]
            対応するground atomのindex (int)
        """
        return self.mrf.gndAtoms[name].idx

    def getGndFormula(self, idx):
        return self.mrf.gndFormulas[idx]

    def getFormula(self, idx):
        return self.mrf.formulas[idx]

    def getGndBlock(self, idx):
        u"""
        [Return]
            block -- idxのブロックに属しているgndAtomIdxのリスト
        """
        idxGA, block = self.mrf.pllBlocks[idx]
        if type(block) != type(None):
            return block
        else:
            return [idxGA]

    def getFormulaWeight(self, idx):
        return self.getFormula(idx).weight

    def setFormulaWeight(self, idx, new_weight):
        self.getFormula(idx).weight = new_weight

    def setFormulaWeights(self, new_weights):
        for i_formula in range(self.getNFormula()):
            self.setFormulaWeight(i_formula, new_weights[i_formula])

    def gndAtom2GndFormulas(self, gndAtomIdx):
        gf_idxs = self._gndAtom2gndFormulas[gndAtomIdx]
        return gf_idxs

    def gndFormula2GndAtoms(self, gndFormulaIdx):
        ga_idxs = self.mrf.gndFormulas[gndFormulaIdx].idxGroundAtoms()
        return ga_idxs

    def gndFormula2Formula(self, gndFormulaIdx):
        gndFormula = self.mrf.gndFormulas[gndFormulaIdx]
        formulaIdx = gndFormula.idxFormula
        return formulaIdx

    def iterGroundings(self, idxFormula):
        gndInfo = self.formula_groundings
        mrf = self.mrf

        formula = mrf.formulas[idxFormula]
        if gndInfo.has_key(idxFormula):
            for assignment in gndInfo[idxFormula]:
                referencedGndAtoms = []
                gndFormula = formula.ground(mrf, assignment, referencedGndAtoms, mrf.simplify)
                if isinstance(gndFormula, FOL.TrueFalse):
                    continue
                gndFormula.isHard = formula.isHard
                gndFormula.weight = formula.weight
                gndFormula.idxFormula = idxFormula
                yield (gndFormula, referencedGndAtoms)
        else:
            #ユーザ指定が無いときはそのまま従来通りの方法で展開する
            for gndFormula, referencedGndAtoms in formula.iterGroundings(mrf, mrf.simplify):
                if isinstance(gndFormula, FOL.TrueFalse):
                    continue
                gndFormula.isHard = formula.isHard
                gndFormula.weight = formula.weight
                gndFormula.idxFormula = idxFormula
                yield (gndFormula, referencedGndAtoms)

    def formula2GndFormulas(self, formulaIdx):
        #gndFormulas = self.mrf.formulas[formulaIdx].iterGroundings(self.mrf)
        gndFormulas = self.iterGroundings(formulaIdx)
        # print self._gndFormulaStr2Index.keys()
        # print "~~~~~~~~~~~~~~~~~~"
        # for gf in gndFormulas:
        #     print str(gf[0])
        gndFormulaIdxs = [self._gndFormulaStr2Index[str(gf)] for gf,ref in gndFormulas]
        return gndFormulaIdxs

    def gndAtom2GndBlock(self, gndAtomIdx):
        return self.mrf.atom2BlockIdx[gndAtomIdx]

    def gndAtom2gndAtomsInSameBlock(self, gndAtomIdx):
        if not (gndAtomIdx in self.mrf.gndBlockLookup.keys()):
            return []
        blockName = self.mrf.gndBlockLookup[gndAtomIdx]
        return self.mrf.gndBlocks[blockName]

    def getNumberOfFormulaGrounding(self, formulaIdx):
        return self._n_ofFormulaGrounding[formulaIdx]

    def applyEvidence(self, evidence, state):
        u"""
        stateがevidenceを満たすように変更する
        stateを内部で変更
        [Keyword arguments]
            evidence -- gndAtomStrをキーとし、True or Falseを値に持つ辞書
            state -- 真理値のリスト
        """
        #evidenceによりFalseとしてあるatomIdx
        blockingIdx = []
        #既にひとつのtrue evidenceが割り当てられたblockのidx
        alreadyFixBlock = []

        #evidenceを適用していく
        for gndAtomStr in evidence.keys():
            gndAtomIdx = self.getGndAtomIdxByStr(gndAtomStr)
            gndBlockIdx = self.gndAtom2GndBlock(gndAtomIdx)
            gndBlock = self.getGndBlock(gndBlockIdx)
            #blockに属していない場合
            if len(gndBlock) == 1:
                state[gndAtomIdx] = evidence[gndAtomStr]
            #blockに属している時
            else:
                #新しいevidenceが真の時は、他のblockのatomをすべてfalseにする
                if evidence[gndAtomStr]:
                    assert not (gndBlockIdx in alreadyFixBlock), '%s'%(gndAtomStr,)
                    state[gndAtomIdx] = True
                    alreadyFixBlock.append(gndBlockIdx)
                #そうでない時はとりあえず自分をFalseにするだけ
                else:
                    blockingIdx.append(gndAtomIdx)
                    state[gndAtomidx] = False

        #すべてのblockについて一つのみTrueとなるようにする
        #もし違反しているところがある場合は最後にTrueだったatomのみ残すか、ランダムにtrueにする
        blockingIdx = set(blockingIdx)
        for blockIdx in range(self.getNGndBlock()):
            #既にfixしているところはいじらない
            if blockIdx in alreadyFixBlock:
                continue
            #blockでない場合はいじらない
            blockAtomIdxs = self.getGndBlock(blockIdx)
            if len(blockAtomIdxs) == 1:
                continue
            #trueとなっているところの数を数える
            t_count = 0
            lastIdx = None
            for atomIdx in blockAtomIdxs:
                if state[atomIdx]:
                    t_count += 1
                    lastIdx = atomIdx
                    state[atomIdx] = False
            #
            if t_count == 0:
                availableIdx = list(set(blockAtomIdxs) - blockingIdx)
                assert len(availableIdx)>0, '%s'%(str(self.getGndAtom(atomIdx[0])), )
                t_idx = np.random.choice(availableIdx)
                state[t_idx] = True
            elif t_count == 1:
                state[lastIdx] = True
            else:
                state[lastIdx] = True

    def countNumTrueGroundingFormula(self, state_array):
        u"""
        state_array -- i列目にi番目のgrounding atomの真理値を含むような二次元行列
        """
        state_array = state_array.astype(bool)

        n_formula = len(self.mrf.formulas)
        count = np.zeros(n_formula)

        n_chain = state_array.shape[0]
        for n in xrange(n_chain):
            #FOLの内部でisによる条件分岐が行われており、np.boolとboolが違うと正しく機能しないため
            state = [True if b else False for b in state_array[n,:]]
            for gndFormula in self.mrf.gndFormulas:
                if gndFormula.isTrue(state):
                    count[gndFormula.idxFormula] += 1.0

        return count / n_chain

    def checkStateCorrectness(self, state, verbose=False):
        u"""
        stateにおいて、各ブロックに一つづつTrueが割り当てられているかを確認する

        正しければTrueを返す
        """
        flag = True
        for i_block in range(self.getNGndBlock()):
            gndBlock = self.getGndBlock(i_block)
            if len(gndBlock) == 1:
                #block with single gndAtom
                pass
            else:
                gndAtoms = [self.getGndAtom(i_gndAtom) for i_gndAtom in gndBlock]
                tv = [gndAtom.isTrue(state) for gndAtom in gndAtoms]
                tv_count = tv.count(True)
                if not (tv_count == 1):
                    flag = False
                    if verbose:
                        print 'illegal block: ',",".join([str(x) for x in gndAtoms])," : ",tv
        return flag

    def calcMarkovBranketProb(self, state, gndAtomIdx):
        u"""
        自分以外のground atomの値を固定した時の、あるground atomがとる

        stateはboolが入っているリスト

        [Return]
            (p0,p1,formulaDelta)
            p0,p1 -- それぞれ0,1となる確率
            formulaDelta -- gndFormulaIdxをキーとする辞書で、現在の値から値をflipさせたときに、どのgndFormulaの値が増える(1)か減る(-1)か変わらない(0)かを示す
        """
        #blockに属していない時
        gndFormulas = self.gndAtom2GndFormulas(gndAtomIdx)
        FormulaWeights = np.array([self.getFormulaWeight(self.gndFormula2Formula(gndFormula)) for gndFormula in gndFormulas])

        currentState = state[gndAtomIdx]
        #0のとき
        state[gndAtomIdx] = False
        gndFormulasVal0 = np.array([self.getGndFormula(gndFormula).isTrue(state) for gndFormula in gndFormulas], dtype=float)
        sum0 = np.sum(gndFormulasVal0 * FormulaWeights)
        #1のとき
        state[gndAtomIdx] = True
        gndFormulasVal1 = np.array([self.getGndFormula(gndFormula).isTrue(state) for gndFormula in gndFormulas], dtype=float)
        sum1 = np.sum(gndFormulasVal1 * FormulaWeights)
        #
        meanSUM = 0.5 * (sum0 + sum1)
        sum0 = np.exp(sum0 - meanSUM)
        sum1 = np.exp(sum1 - meanSUM)
        #
        state[gndAtomIdx] = currentState
        #
        formulaDelta = None
        if currentState:
            formulaDelta = gndFormulasVal0 - gndFormulasVal1
            formulaDelta = {gndFormula:delta for gndFormula,delta in zip(gndFormulas, formulaDelta)}
        else:
            formulaDelta = gndFormulasVal1 - gndFormulasVal0
            formulaDelta = {gndFormula:delta for gndFormula,delta in zip(gndFormulas, formulaDelta)}
        return sum0 / (sum0 + sum1), sum1 / (sum0 + sum1), formulaDelta

    def calcMarkovBranketProbBlock(self, state, gndAtomsIdxInBlock):
        u"""
        自分以外のground atomの値を固定した時の、あるground atomがとる

        gndAtomIdxを含むblockについて擬似尤度を計算する

        stateはboolが入っているリスト

        [Return]
            (ps, formulaDeltas)
            ps -- gndAtomsIdxInBlockの各要素がTrueとなった時の確率のndarray
            formulaDeltas -- 以下に示すような辞書のリスト
                ※gndFormulaIdxをキーとする辞書で、現在の値から値をflipさせたときに、どのgndFormulaの値が増える(1)か減る(-1)か変わらない(0)かを示す
        """
        #blockに属するうちどれか一つ以上のgndAtomの属するformula
        gndFormulas = reduce(lambda x,y:x+y, [self.gndAtom2GndFormulas(gndAtomIdx) for gndAtomIdx in gndAtomsIdxInBlock])
        gndFormulas = list(set(gndFormulas))
        FormulaWeights = np.array([self.getFormulaWeight(self.gndFormula2Formula(gndFormula)) for gndFormula in gndFormulas])

        currentTrueIdx = None
        for i,gndAtomIdx in enumerate(gndAtomsIdxInBlock):
            if state[gndAtomIdx]:
                currentTrueIdx = i
                break
        assert type(currentTrueIdx) != type(None)

        #
        ps = []
        formulaVals = []
        weightExpSums = []

        state[gndAtomsIdxInBlock[currentTrueIdx]] = False
        for i in range(len(gndAtomsIdxInBlock)):
            #自分のところのみTrueとする
            if i>0:
                state[gndAtomsIdxInBlock[i-1]] = False
            state[gndAtomsIdxInBlock[i]] = True

            #
            formulaVal = np.array([self.getGndFormula(gndFormula).isTrue(state) for gndFormula in gndFormulas], dtype=float)
            formulaVals.append(formulaVal)
            weightExpSum = np.sum(formulaVal * FormulaWeights)
            weightExpSums.append(weightExpSum)
        state[gndAtomsIdxInBlock[-1]] = False
        state[gndAtomsIdxInBlock[currentTrueIdx]] = True

        formulaVals = np.array(formulaVals)
        weightExpSums = np.array(weightExpSums)
        weightExpSums -= np.mean(weightExpSums)
        weightExpSums = np.exp(weightExpSums)

        #正規化して確率とする
        ps = weightExpSums / np.sum(weightExpSums)
        #現在のstateとそれぞれflipしたものについてgndFormulaValを比較する
        formulaDeltasRaw = formulaVals[:,:] - formulaVals[np.newaxis,currentTrueIdx,:]

        formulaDeltas = []
        for i in range(len(gndAtomsIdxInBlock)):
            formulaDelta = {gndFormula:delta for gndFormula,delta in zip(gndFormulas, formulaDeltasRaw[i])}
            formulaDeltas.append(formulaDelta)

        return ps, formulaDeltas

    def calcPseudodLdW(self, states):
        u"""
        尤度下限のMLNパラメータについて、擬似尤度の近似をおいて微分を計算する
        E_q(Y) [ ni(Y) - sum(l, pw(Xl=0|MB) * ni(Y[Yl=0])  +  pw(Xl=1|MB) * ni(Y[Yl=1]) ) ]

        [Keyword Arguments]
            states -- 二次元リストでTrueかFalseを要素とする

        [Return]
            (n_formula,)のndarray
        """
        n_chain = len(states)

        n_formula = len(self.mrf.formulas)
        count = np.zeros((n_chain, n_formula))

        for n in xrange(n_chain):
            skip_lst = []
            for gndAtomIdx in xrange(self.getNGndAtom()):
                #blockに属していない時
                if len(self.gndAtom2gndAtomsInSameBlock(gndAtomIdx))==0:
                    p0,p1,deltaGndFormula = self.calcMarkovBranketProb(states[n], gndAtomIdx)
                    p = p0 if states[n][gndAtomIdx] else p1
                    for gndFormulaIdx,val in zip(deltaGndFormula.keys(), deltaGndFormula.values()):
                        formulaIdx = self.gndFormula2Formula(gndFormulaIdx)
                        count[n,formulaIdx] -= p * val
                #blockに属しているがすでに他のatom経由で考慮されている時
                elif gndAtomIdx in skip_lst:
                    pass
                else:
                    sameBlockAtomIdxs = self.gndAtom2gndAtomsInSameBlock(gndAtomIdx)
                    ps, formulaDeltas = self.calcMarkovBranketProbBlock(states[n], sameBlockAtomIdxs)

                    for p,formulaDelta in zip(ps,formulaDeltas):
                        for gndFormulaIdx,val in zip(formulaDelta.keys(), formulaDelta.values()):
                            formulaIdx = self.gndFormula2Formula(gndFormulaIdx)
                            count[n,formulaIdx] -= p * val

                    skip_lst += sameBlockAtomIdxs

        ret = np.sum(count, axis=0) / n_chain
        return ret

def softmax(x):
    x = np.array(x)
    assert x.ndim==1
    x = x - np.max(x)
    px = np.exp(x)
    return px / np.sum(px)

class MLN_Block_Sampler(object):
    u"""
    MLNのあるBlockに対してサンプリングを行う

    X -- MLN内部のノード
    Y -- 今回サンプリングを行うBlock
    Z -- Blockに結合している他の確率モデルp(Z|Y)のノード

    p(Y|X,Z) \sim p(Z|Y)p(Y|X)
    therefore
    log p(Y|X,Z) = log p(Z|Y) + log p(Y|X) + const.
    """
    def __init__(self, mln):
        self.mln = mln

        x = T.dvector('x')
        self._softmax = softmax

    def softmax(self, x):
        return self._softmax(x)[0]

    def MLN_L0L1(self, state, gndAtomIdx):
        u"""
        単一のノードからなるgndAtom yについて、y=0とy=1の時のlog p(Y|X;MLN)を求める
        （ただし、y=0とy=1において共通の-log(Z(w))は除く）

        Li = sum_(gndFormulas which contains gndAtom)(w_j f_j(y=i))

        [Return]
            L -- L1 - L0 を返す
        """
        gndFormulas = self.mln.gndAtom2GndFormulas(gndAtomIdx)

        #現在の値を保持しておく
        backup_val = state[gndAtomIdx]

        L0, L1 = 0.0, 0.0
        for i_gndFormula in gndFormulas:
            gndFormula = self.mln.getGndFormula(i_gndFormula)
            weight = self.mln.getFormulaWeight(self.mln.gndFormula2Formula(i_gndFormula))

            #Y=0
            state[gndAtomIdx] = False
            if gndFormula.isTrue(state):
                L0 += weight
            #Y=1
            state[gndAtomIdx] = True
            if gndFormula.isTrue(state):
                L1 += weight

        state[gndAtomIdx] = backup_val

        return L1 - L0

    def MLN_Li(self, state, gndAtomIdxs):
        u"""
        複数のノードを含むBlockについて、それぞれのYのみが1であった時の log p(Y|X;MLN)を求める
        (ただし共通の-log(Z(w))は除く)

        Li = sum_(gndFormulas which contains gndAtom)(w_j f_j(y=i))

        [Keyword arguments]
            gndAtomIdxs -- gndAtomのindexのリスト
        [Return]
            Li -- 各ノードに対応するlog pが納められたndarray
        """
        #blockの各ノードに結合するgndFormulaの和集合
        gndFormulas = []
        for gndAtomIdx in gndAtomIdxs:
            gndFormulas.extend(self.mln.gndAtom2GndFormulas(gndAtomIdx))
        gndFormulas = np.unique(gndFormulas)

        #それぞれのノードにおけるlog p
        n_nodes = len(gndAtomIdxs)
        Li = np.zeros(n_nodes)

        #現在TrueとなっているnodeのgndAtomIdxsにおけるindexを保持しておく
        backup_index = None
        for i,gndAtomIdx in enumerate(gndAtomIdxs):
            if state[gndAtomIdx]:
                backup_index = i
        current_index = [backup_index]

        #indexのみTrueにする
        def flip(index):
            state[gndAtomIdxs[index]] = True
            state[gndAtomIdxs[current_index[0]]] = False
            current_index[0] = index

        for i_gndFormula in gndFormulas:
            gndFormula = self.mln.getGndFormula(i_gndFormula)
            weight = self.mln.getFormulaWeight(self.mln.gndFormula2Formula(i_gndFormula))

            for i_node in range(n_nodes):
                flip(i_node)
                if gndFormula.isTrue(state):
                    Li[i_node] += weight

        flip(backup_index)

        return Li

    def probBlock(self, state, gndAtomIdxs, otherL):
        u"""
        他の確率モデルによるlogp(Z|Y)と合わせて、Blockの各ノードの値が1となる確率を計算する

        [Keyword arguments]
            otherL -- blockに属するノードの個数と等しい要素数を持つリスト もしくは、ノード数が1の時は以下のようなスカラー
            ※ノード数1の時は、Y=0となるときのlogpが0になるように調整されたY=1のときのlogp つまりlog(p1/p0)
        [Return]
            各ノードが1となる確率のリスト　もしくは　ノード数が1のときはそのノードが1となる確率
        """
        #gndAtomIdxs = mln.getGndBlock(blockIdx)
        if len(gndAtomIdxs) == 1:
            L = self.MLN_L0L1(state, gndAtomIdxs[0]) + otherL
            return self.softmax([0.0,L])[1]
        else:
            L = self.MLN_Li(state, gndAtomIdxs) + np.array(otherL)
            return self.softmax(L)

    def updateBlock(self, state, gndAtomIdxs, otherL):
        u"""
        他の確率モデルによるlogp(Z|Y)からBlockの各ノードの値をサンプリングする
        """
        #gndAtomIdxs = mln.getGndBlock(blockIdx)
        if len(gndAtomIdxs) == 1:
            p = self.probBlock(state, gndAtomIdxs, otherL)

            gndAtomIdx = gndAtomIdxs[0]
            state[gndAtomIdx] = np.random.uniform() < p
        else:
            ps = self.probBlock(state, gndAtomIdxs, otherL)

            for gndAtomIdx in gndAtomIdxs:
                state[gndAtomIdx] = False
            idx = np.random.choice(len(gndAtomIdxs), p = ps)
            state[gndAtomIdxs[idx]] = True

class MCSAT_Sampler(object):
    u"""
    要注意：
        evidenceのみ更新した場合はinitWithEvidence()を行い、stateの方も手動で(evidenceのところのみ)更新すること(mln.applyEvidenceなどを使うこと)
        重みを更新した場合はinit(),initWithEvidence()を両方行う必要性
    """
    def __init__(self, mrf):
        self.mrf = mrf

        self.n_gndAtom = len(self.mrf.gndAtoms)

    def _initKB(self):
        #toCNF
        #convert the given ground formulas to CNF
        #if allPositive=True, then formulas with negative weights are negated to make all weights positive
        #@return a new pair (gndFormulas, formulas)
        self.gndFormulas, self.formulas = toCNF(self.mrf.gndFormulas, self.mrf.formulas, allPositive=True)

        # get clause data
        self.gndFormula2ClauseIdx = {} # ground formula index -> tuple (idxFirstClause, idxLastClause+1) for use with range
        self.clauses = [] # list of clauses, where each entry is a list of ground literals
        #self.GAoccurrences = {} # ground atom index -> list of clause indices (into self.clauses)
        idxClause = 0
        # process all ground formulas
        for idxGndFormula, f in enumerate(self.gndFormulas):
            # get the list of clauses
            if type(f) == FOL.Conjunction:
                lc = f.children
            else:
                lc = [f]
            self.gndFormula2ClauseIdx[idxGndFormula] = (idxClause, idxClause + len(lc))
            # process each clause
            for c in lc:
                if hasattr(c, "children"):
                    lits = c.children
                else: # unit clause
                    lits = [c]
                # add clause to list
                self.clauses.append(lits)
                # next clause index
                idxClause += 1

    def _getEvidenceBlockData(self, evidence):
        u"""
        evidenceはgndAtomStrをキーとし、True or Falseを値とする辞書
        """
        #evidenceを設定 (clear=Trueは本当は必要ないが一応書いておく)
        self.mrf.setEvidence(evidence, clear = True)
        #blockについての情報を抽出
        self.evidenceBlocks = [] # list of pll block indices where we know the true one (and thus the setting for all of the block's atoms)
        self.blockExclusions = {} # dict: pll block index -> list (of indices into the block) of atoms that mustn't be set to true
        for idxBlock, (idxGA, block) in enumerate(self.mrf.pllBlocks): # fill the list of blocks that we have evidence for
            if block != None:
                haveTrueone = False
                falseones = []
                for i, idxGA in enumerate(block):
                    ev = self.mrf._getEvidence(idxGA, False)
                    if ev == True:
                        haveTrueone = True
                        break
                    elif ev == False:
                        falseones.append(i)
                if haveTrueone:
                    self.evidenceBlocks.append(idxBlock)
                elif len(falseones) > 0:
                    self.blockExclusions[idxBlock] = falseones
            else:
                if self.mrf._getEvidence(idxGA, False) != None:
                    self.evidenceBlocks.append(idxBlock)

    def _setRandomState(self, state, blockInfo=None):
        u"""
            既知のブロック変数に注意しながら値を初期化する
            evidenceBlocksに含まれる変数は更新しない ※単一変数は無条件で evidenceBlocks
        """
        if state == []: #no evidence given -> initialize list, elements are overwritten below
            for i in range(len(self.mrf.gndAtoms)): state.append(None)

        mln = self.mrf
        for idxBlock, (idxGA, block) in enumerate(mln.pllBlocks):
            if idxBlock not in self.evidenceBlocks:
                if block != None: # block of mutually exclusive atoms
                    blockExcl = self.blockExclusions.get(idxBlock)
                    if blockExcl == None:
                        chosen = block[random.randint(0, len(block) - 1)]
                        for idxGA in block:
                            state[idxGA] = (idxGA == chosen)
                        if blockInfo != None:
                            falseOnes = filter(lambda x: x != chosen, block)
                            blockInfo[idxBlock] = [chosen, falseOnes]
                    else:
                        choosable = []
                        for i, idxGA in enumerate(block):
                            if i not in blockExcl:
                                choosable.append(idxGA)
                        maxidx = len(choosable) - 1
                        if maxidx <= 0:
                            raise Exception("Evidence forces all ground atoms in block %s to be false" % mln._strBlock(block))
                        chosen = choosable[random.randint(0, maxidx)]
                        for idxGA in choosable:
                            state[idxGA] = (idxGA == chosen)
                        if blockInfo != None:
                            choosable.remove(chosen)
                            blockInfo[idxBlock] = [chosen, choosable]
                else: # regular ground atom, which can either be true or false
                    chosen = random.randint(0, 1)
                    state[idxGA] = bool(chosen)


    class VirtualInferObject(object):
        def __init__(self, sampler):
            self.sampler = sampler

            self.mrf = sampler.mrf
            self.evidenceBlocks = sampler.evidenceBlocks
            self.blockExclusions = sampler.blockExclusions
            self.clauses = sampler.clauses

        def setRandomState(self, state, blockInfo=None):
            self.sampler._setRandomState(state=state, blockInfo=blockInfo)

    def _satisfySubset(self, p, state, **args):
        u"stateを基に次のステップで満たされるべき論理式を探す"
        M = []
        NLC = []
        for idxGF, gf in enumerate(self.gndFormulas):
            if gf.isTrue(state):
                    u = random.uniform(0, math.exp(self.wt[gf.idxFormula]))
                    if u > 1:
                        if gf.isLogical():
                            clauseRange = self.gndFormula2ClauseIdx[idxGF]
                            M.extend(range(*clauseRange))
                        else:
                            NLC.append(gf)
        #指定した論理式を満たす状態をサンプルする
        ss = SampleSAT(self.mrf, state, M, NLC, self.vInferObject, p=p, **args)
        ss.run()

    def init(self):
        #initialize the KB and gather required info
        self._initKB()
        self.wt = [f.weight for f in self.formulas]

    def initWithEvidence(self, evidence=None):
        u"""
        evidenceに依存する初期化処理を行う evidenceが変わるごとに実行する必要がある

        evidenceはgndAtomStrをキーとし、True or Falseを値とする辞書
        """
        if type(evidence) == type(None):
            evidence = {}

        #blockについての情報（どのブロックが真理値定めるか、一部だけ定まってるかの情報）
        #内部でmrf.evidenceを操作しているので、バックアップもとりつつ
        oldEvidence = copy.copy(self.mrf.evidence)
        self._getEvidenceBlockData(evidence)
        self.mrf.evidence = oldEvidence

        self.vInferObject = MCSAT_Sampler.VirtualInferObject(self)

    def initState(self, state=None):
        u"""
        配列stateを初期化する
        """
        if type(state) == type(None):
            state = [False]*self.n_gndAtom

        M = []
        NLC = []
        for idxGF, gf in enumerate(self.gndFormulas):
            #重みが20以上の論理式はlogicalな制約とする
            if self.wt[gf.idxFormula] >= 20:
                if gf.isLogical():
                    clauseRange = self.gndFormula2ClauseIdx[idxGF]
                    M.extend(range(*clauseRange))
                else:
                    NLC.append(gf)
        ss = SampleSAT(self.mrf, state, M, NLC, self.vInferObject, p=0.5)
        ss.run()

        return state

    def initialize(self, state, mln, evidence, ifEvidenceChanged = False, ifWeightUpdated = False):
        u"""
        新しくMC-SATのchainを始めるときに実行する
        [Keyword arguments]
            state -- 初期state
            mln -- MLNインスタンス
            evidence -- hard evidence
            ifEvidenceChanged -- hard evidenceが前回から変化した場合に設定する
            ifWeightUpdated -- 重みが前回から変化した場合に設定する (もしくはこのsamplerを初めて使うとき)
        """
        if ifWeightUpdated:
            self.init()

        if ifWeightUpdated or ifEvidenceChanged:
            mln.applyEvidence(evidence, state)
            self.initWithEvidence(evidence)

        state = self.initState(state)
        return state


    def updateState(self, state, p=0.5, **args):
        u"""
        MC-SATに従いstateを更新する

        ※initWithEvidenceを用いてもevidenceの変数が指定されるだけでstateは更新されないので、別途stateも更新する必要がある

        maxIterを指定するとMC-SATにおける最大試行回数を設定できる
        """
        self._satisfySubset(p, state, **args)

        return state

if __name__=="__main__":
#     PROG_STR = """
# inst={0,1,2,3,4,5}
# val={0,1,2}
#
# A(inst,val)
# B(inst,val)
#
# 1.0 A(i,v)
# 7.0 A(i,v) => B(i,v)
# """
#
#     mln = MLN(prog_str = PROG_STR)
#
#     sampler = MCSAT_Sampler(mln.mrf)
#     sampler.init()
#     sampler.initWithEvidence({'A(0,0)': False})
#
#     state = sampler.initState()
#     chain = []
#     for i in xrange(5000):
#         #if random.uniform(0.0,1.0) < 0.3:
#         #    state[mln.getGndAtomIdxByStr('A(0,0)')] = True
#         #    sampler.initWithEvidence({'A(0,0)':True})
#         #else:
#         #    state[mln.getGndAtomIdxByStr('A(0,0)')] = False
#         #    sampler.initWithEvidence({'A(0,0)':False})
#         state = sampler.updateState(state)
#         chain.append(copy.copy(state))
#     #ランダムにevidenceを設定した変数がうまく動いていない
#     #おそらく初期値で固定されてしまっている
#     #SampleSATの中身を勉強する必要あり
#
#     chain = np.array(chain, dtype=float)
#
#     print 'shape[0]: ', chain.shape[0]
#     print 'A(0,0): ', mln.getGndAtomIdxByStr('A(0,0)')
#     print 'B(0,0): ', mln.getGndAtomIdxByStr('B(0,0)')
#     print np.sum(chain[:,mln.getGndAtomIdxByStr('A(0,0)')] * chain[:,mln.getGndAtomIdxByStr('B(0,0)')])
#     print np.sum(chain[:,mln.getGndAtomIdxByStr('A(0,0)')])
#
#     print 'est. probability'
#     print np.sum(chain, axis=0) / chain.shape[0]
#
#     #for i,v in zip(mln.mrf.gndAtoms.keys(), mln.mrf.gndAtoms.values()):
#     #    print type(i),type(v),v.idx
#
#     #for i_gf in range(mln.getNGndFormula()):
#     #    print mln.getGndFormula(i_gf).isTrue(state)
#
#     print state

#     PROB_STR = """
# val = {0,1,2}
#
# A(val!)
#
# %f A(0)
# %f A(1)
# %f A(2)
# """%(np.log(2), np.log(7), 0.0)
#     print PROB_STR
#     mln = MLN(prog_str=PROB_STR)
#
#     print 'block 0'
#     print mln.getGndBlock(0)
#
#     gndAtomIdxs = [mln.getGndAtomIdxByStr('A(%d)'%i) for i in range(3)]
#     print 'gndAtomIdxs'
#     print gndAtomIdxs
#
#     mcsat_sampler = MCSAT_Sampler(mln.mrf)
#     mcsat_sampler.init()
#     mcsat_sampler.initWithEvidence()
#
#     states = []
#     state = mcsat_sampler.initState()
#     for epoch in xrange(1000):
#         state = mcsat_sampler.updateState(state)
#         states.append(copy.copy(state))
#     states = np.array(states)
#
#     prob = np.sum(states, axis=0) / 1000.0
#     print 'prob'
#     print prob
#
#     blockSampler = MLN_Block_Sampler(mln)
#     print blockSampler.probBlock(state=[True, False, False], blockIdx=0, otherL=[np.log(7.0),np.log(2.0),0.0])

#     PROB_STR = """
# val = {0}
#
# A(val)
#
# %f A(0)
# """%(np.log(0.7/0.3),)
#     print PROB_STR
#     mln = MLN(prog_str=PROB_STR)
#
#     print 'block 0'
#     print mln.getGndBlock(0)
#
#     blockSampler = MLN_Block_Sampler(mln)
#     print blockSampler.probBlock(state=[True], blockIdx=0, otherL=np.log(0.7/0.3))

#     PROB_STR = """
# inst={%s}
#
# A(inst)
#
# 0.0 A(i)
#     """%(",".join([str(x) for x in range(500)]), )
#
#     mln = MLN(prog_str=PROB_STR)
#
#     state = [False] * mln.getNGndAtom()
#     for i in range(500):
#         state[i] = np.random.uniform() < 0.8
#
#     states = [state]
#
#     adam = Adam(np.array([0.0]), alpha=0.1)
#     for epoch in xrange(10000):
#         dLdW = mln.calcPseudodLdW(states)
#         adam.update(-dLdW)
#         mln.setFormulaWeight(0, adam.getParam()[0])
#
#         print adam.getParam()[0]

    #以下本体
    N = 1000
    PROB_STR = """
inst={%s}
val={0,1,2}

A(inst,val!)

%f A(i,1)
%f A(i,2)
    """%(",".join([str(x) for x in range(N)]), 0.0, 0.0)

    S = np.random.choice(3,size=N,p=[0.1,0.2,0.7])

    mln = MLN(prog_str=PROB_STR)
    for i in range(mln.getNFormula()):
        print i, mln.getFormula(i)
    try:
        input('proceed?')
    except SyntaxError:
        pass

    state = [False] * mln.getNGndAtom()
    for n in range(N):
        if S[n]==1:
            state[mln.getGndAtomIdxByStr('A(%d,1)'%n)] = True
        elif S[n]==2:
            state[mln.getGndAtomIdxByStr('A(%d,2)'%n)] = True
        else:
            state[mln.getGndAtomIdxByStr('A(%d,0)'%n)] = True
    states = [state]

    ####################
    #グラフ描画
    # import matplotlib.pyplot as plt
    #
    # bu_w1 = mln.getFormulaWeight(0)
    # bu_w2 = mln.getFormulaWeight(1)
    #
    # @np.vectorize
    # def calcGrad(w1, w2):
    #     mln.setFormulaWeight(0,w1)
    #     mln.setFormulaWeight(1,w2)
    #
    #     dw = mln.calcPseudodLdW(states)
    #     return dw[0]/N,dw[1]/N
    #
    # W1,W2 = np.meshgrid(np.linspace(0.0,3.0,100), np.linspace(0.0,3.0,100))
    # p1,p2 = np.sum(S==1)/float(N), np.sum(S==2)/float(N)
    # print 'actual prob'
    # print p1,p2
    #
    # #目的関数のグラフ
    # L = - np.log(np.exp(W1) + np.exp(W2) + 1.0) + p1 * W1 + p2 * W2
    # plt.figure()
    # CS = plt.contour(W1, W2, L, 20, colors='k')
    # plt.clabel(CS, fontsize=9, inline=1)
    # #plt.show()
    #
    # #勾配のグラフ
    # G1 = p1 - np.exp(W1) / (1.0 + np.exp(W1) + np.exp(W2))
    # G2 = p2 - np.exp(W2) / (1.0 + np.exp(W1) + np.exp(W2))
    #
    # #plt.figure()
    # # CS = plt.contour(W1, W2, G1, 6,
    # #              colors='r',  # negative contours will be dashed by default
    # #              )
    # # plt.clabel(CS, fontsize=9, inline=1)
    # #
    # # CS = plt.contour(W1, W2, G2, 6,
    # #              colors='b',  # negative contours will be dashed by default
    # #              )
    # # plt.clabel(CS, fontsize=9, inline=1)
    #
    # plt.quiver(W1[::5,::5], W2[::5,::5], G1[::5,::5], G2[::5,::5],
    #        units='xy', scale=3., zorder=3, color='b',
    #        width=0.007, headwidth=3., headlength=4.)
    #
    # #擬似尤度による勾配
    # print 'start calculating pseudo grad map'
    # PG1,PG2 = calcGrad(W1[::5,::5],W2[::5,::5])
    # print 'finish calculation'
    #
    # plt.quiver(W1[::5,::5], W2[::5,::5], PG1, PG2,
    #            units='xy', scale=3., zorder=3, color='r',
    #            width=0.007, headwidth=3., headlength=4.)
    #
    # plt.show()
    #
    # #２つの勾配の残差をプロット
    # plt.figure()
    # CS = plt.contour(W1, W2, L, 20, colors='k')
    # plt.clabel(CS, fontsize=9, inline=1)
    # plt.quiver(W1[::5,::5], W2[::5,::5], PG1-G1[::5,::5], PG2-G2[::5,::5],
    #        units='xy', scale=3., zorder=3, color='b',
    #        width=0.007, headwidth=3., headlength=4.)
    # plt.show()
    #
    # try:
    #     input('proceed?')
    # except SyntaxError:
    #     pass
    #
    # mln.setFormulaWeight(0, bu_w1)
    # mln.setFormulaWeight(1, bu_w2)
    #######################
    #以下通常タスク

    adam = Adam(np.array([mln.getFormulaWeight(i_formula) for i_formula in range(mln.getNFormula())]), alpha=0.1)
    for epoch in xrange(100):
        dLdW = mln.calcPseudodLdW(states)
        adam.update(-dLdW)
        mln.setFormulaWeight(0, adam.getParam()[0])
        mln.setFormulaWeight(1, adam.getParam()[1])

        #plt.scatter([adam.getParam()[0]], [adam.getParam()[1]])

        print dLdW/N
        print adam.getParam(), [np.log(2.0), np.log(7.0)]
    #plt.show()
