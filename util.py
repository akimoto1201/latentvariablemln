# coding: UTF-8

import numpy as np

class Adam(object):
    u"""
    関数を最小化していく
    尤度最大化したいときはgradに負をつけて最小化問題にしないといけないので注意

    [A Neelakantan] Adding Gradient Noise Improves Learning for very deep networks
    を使ってランダム・ノイズを追加するかを選べる
    """
    def __init__(self, init, alpha=0.001, beta1 = 0.9, beta2=0.999, epsilon=1e-8, addNoise = False, eta=0.001, gamma=0.55):
        u"""
        init -- ndarray
        """
        self.alpha = alpha
        self.beta1 = beta1
        self.beta2 = beta2
        self.epsilon = epsilon

        self.shape = init.shape

        self.theta = init
        self.m = np.zeros(self.shape)
        self.v = np.zeros(self.shape)
        self.t = 0

        self.addNoise = addNoise
        self.eta = eta
        self.gamma = gamma

    def update(self, g):
        self.t += 1
        self.m = self.beta1 * self.m + (1.-self.beta1) * g
        self.v = self.beta2 * self.v + (1.-self.beta2) * np.power(g,2)
        m_hat = self.m / (1.0 - self.beta1 ** self.t)
        v_hat = self.v / (1.0 - self.beta2 ** self.t)

        noise = np.random.normal(0.0, self.eta / (1+self.t)**self.gamma, self.shape) if self.addNoise else 0.0

        self.theta = self.theta - self.alpha * np.divide(m_hat, np.sqrt(v_hat) + self.epsilon) + noise

    def getParam(self):
        return self.theta