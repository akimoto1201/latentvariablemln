# coding: UTF-8

u"""
TimeSeries4ディレクトリから実行すること
"""

import sys

from util import Adam

from base import MLN, MCSAT_Sampler, softmax

import numpy as np
import copy

#import matplotlib.pyplot as plt

import time

from alchemy import learnMLNviaAlchemy, learnWt_Alchemy

class ModelBinBinary(object):
    def __init__(self, n_bin, init_p0, init_p1, prior1 = 0.5, learnPrior = True):
        u"""
        [Keyword arguments]
            n_bin -- Xの値のドメイン数
            init_p0, init_p1 -- (n_bin,)のndarray それぞれp(X=i|Y=False), p(X=i|Y=True)
        """
        self._p0 = init_p0 / np.sum(init_p0)
        self._p1 = init_p1 / np.sum(init_p1)
        self._n_bin = n_bin

        self._prior1 = prior1
        self._learnPrior = learnPrior

    def train(self, X, Y):
        u"""
        [Keyword arguments]
            X -- (n_data,)のint型ndarray データの属するbinを表す
            Y -- (n_data,)のint型ndarray 対応するデータが0か1かを返す
        """
        assert isinstance(X, np.ndarray)
        assert isinstance(Y, np.ndarray)
        assert X.dtype == int
        assert Y.dtype == int

        N0 = np.sum(Y==0)
        N1 = np.sum(Y==1)

        self._p0 = np.array([np.sum(np.logical_and(Y==0, X==x)) / float(N0) for x in range(self._n_bin)])
        self._p1 = np.array([np.sum(np.logical_and(Y==1, X==x)) / float(N1) for x in range(self._n_bin)])

        if self._learnPrior:
            self._prior1 = np.mean(Y==1)

    def calcL(self, X):
        u"""
        [Keyword arguments]
            X -- (n_data,)のint型ndarray データの属するbinを表す
        """
        assert isinstance(X, np.ndarray)
        assert X.dtype == int

        p0 = self._p0[X] * (1.0-self._prior1)
        p1 = self._p1[X] * self._prior1
        return np.log(p1) - np.log(p0)

    def toStr(self):
        ret = "p0:%s\np1:%s\nprior1:%.3f"%(np.array2string(self._p0, precision=3), np.array2string(self._p1, precision=3), self._prior1)
        return ret

class ModelPoissonBinary(object):
    def __init__(self, lam0, lam1, prior1 = 0.5):
        self._lam0 = lam0
        self._lam1 = lam1

        self._prior1 = prior1

    def train(self, X, Y):
        u"""
        [Keyword arguments]
            X -- (n_data,)のndarray データを表す
            Y -- (n_data,)のint型ndarray 0ならTrue, 1ならFalseを表す
        """
        assert isinstance(X, np.ndarray)
        assert isinstance(Y, np.ndarray)
        assert Y.dtype == int

        self._lam0 = np.mean(X[Y==0])
        self._lam1 = np.mean(X[Y==1])

        self._prior1 = np.mean(Y==1)

    def calcL(self, X):
        u"""
        [Keyword arguments]
            X -- (n_data,)のndarray
        """
        L0 = X * np.log(self._lam0) - self._lam0 + np.log(1.0-self._prior1)
        L1 = X * np.log(self._lam1) - self._lam1 + np.log(self._prior1)
        return L1 - L0

    def toStr(self):
        ret = "lam0: %.3f lam1: %.3f orior1:%.3f"%(self._lam0, self._lam1, self._prior1)
        return ret

class ModelGaussBinary(object):
    def __init__(self, init_mu, init_sigma, prior1 = 0.5, train_sigma = True, train_prior = True):
        u"""
        [Keyword arguments]
            init_mu -- (2,n_dim)のndarray 最初の次元は0がFalse,1がTrueクラスのものに対応
            init_sigma -- (2,n_dim,n_dim)のndarray 最初の次元についてはinit_muと同様
        """
        self._n_dim = init_mu.shape[1]
        self._mu = init_mu
        self._sigma = init_sigma

        self._prior1 = prior1

        self._train_sigma = train_sigma
        self._train_prior = train_prior

    def train(self, X, Y):
        u"""
        [Keyword arguments]
            X -- (n_data, n_dim)のndarray データを表す
            Y -- (n_data,)のint ndarray 0,1のいづれかを値として持つ
        """
        assert isinstance(X, np.ndarray)
        assert isinstance(Y, np.ndarray)
        assert Y.dtype == int

        self._mu = np.array([
            np.mean(X[Y==0], axis=0),
            np.mean(X[Y==1], axis=0)
        ])
        if self._train_sigma:
            self._sigma = np.array([
                np.cov(X[Y==0].T),
                np.cov(X[Y==1].T)
            ])
            if self._sigma.ndim == 1:
                self._sigma = self._sigma.reshape((2,1,1))

        if self._train_prior:
            self._prior1 = np.mean(Y==1)

    def calcL(self, X):
        u"""
        [Keyword arguments]
            X -- (n_data, n_dim)のndarray
        """
        L0 = self._calcLogP(X, self._mu[0], self._sigma[0]) + np.log(1.0 - self._prior1)
        L1 = self._calcLogP(X, self._mu[1], self._sigma[1]) + np.log(self._prior1)
        return L1 - L0

    def _calcLogP(self, X, mu, sigma):
        #2*piの部分はmu,sigmaに依存しない定数なので除く
        invSig = np.linalg.inv(sigma)
        _X = X - mu[np.newaxis,:]
        return - 0.5 * (np.sum(np.dot(_X, invSig) * _X, axis=1) + np.log(np.linalg.det(sigma)))

    def toStr(self):
        ret="mu0:\n%s\nsigma0:\n%s\nmu1:\n%s\nsigma1:\n%s\nprior1: %.3f"%(np.array2string(self._mu[0], precision=3), np.array2string(self._sigma[0], precision=3), np.array2string(self._mu[1], precision=3), np.array2string(self._sigma[1], precision=3), self._prior1)
        return ret

class ModelBernouliBinary(object):
    def __init__(self, n_bin, init_p):
        u"""
        [Keyword arguments]
            n_bin -- Xの値域
        """
        self._n_bin = n_bin
        self._p = init_p

    def train(self, X, Y):
        u"""
        [Keyword arguments]
            X -- (n_data,)のint型ndarray データの属するbinを表す
            Y -- (n_data,)のint型ndarray 対応するデータが0か1かを返す
        """
        print [np.sum(X==x) for x in xrange(self._n_bin)]
        self._p = np.array([np.sum(np.logical_and(X==x, Y==1)) / float(np.sum(X==x)) for x in xrange(self._n_bin)])

    def calcL(self, X):
        u"""
        [Keyword arguments]
            X -- (n_data,)のint型ndarray データの属するbinを表す
        """
        l = np.log(self._p[X]) - np.log(1.0 - self._p[X])
        return l

    def toStr(self):
        ret = "p:\t%s"%(np.array2string(self._p, precision=3),)
        return ret

class BinaryModelInterface(object):
    def __init__(self, n_inst, gndAtomIdxs):
        u"""
        [Keyword arguments]
            n_inst -- データ数の次元
            gndAtomIdxs -- 各データに対応する潜在変数のmlnにおけるgndAtomIdxのリスト
        """
        self._n_inst = n_inst
        self._gndAtomIdxs = np.array(gndAtomIdxs)

        self._softmax = softmax

    def setModel(self, model):
        assert hasattr(model, 'calcL')
        self._model = model
        return self

    def _MLN_L0L1(self, state, gndAtomIdx, mln):
        u"""
        単一のノードからなるgndAtom yについて、y=0とy=1の時のlog p(Y|X;MLN)を求める
        （ただし、y=0とy=1において共通の-log(Z(w))は除く）

        Li = sum_(gndFormulas which contains gndAtom)(w_j f_j(y=i))

        [Return]
            L -- L1 - L0 を返す
        """
        gndFormulas = mln.gndAtom2GndFormulas(gndAtomIdx)

        #現在の値を保持しておく
        backup_val = state[gndAtomIdx]

        L0, L1 = 0.0, 0.0
        for i_gndFormula in gndFormulas:
            gndFormula = mln.getGndFormula(i_gndFormula)
            weight = mln.getFormulaWeight(mln.gndFormula2Formula(i_gndFormula))

            #Y=0
            state[gndAtomIdx] = False
            if gndFormula.isTrue(state):
                L0 += weight
            #Y=1
            state[gndAtomIdx] = True
            if gndFormula.isTrue(state):
                L1 += weight

        state[gndAtomIdx] = backup_val

        return L1 - L0

    def sample(self, state, mln, X):
        u"""
        現在のmodelとmlnの重みに従い、このInterfaceが扱うground atomの真理値をgibbs sampleによりサンプルし、stateに格納する
        [Keyword arguments]
            state -- boolを格納するリスト
            mln -- MLNインスタンス
            X -- modelに渡すデータndarray
        """
        #model側の尤度を計算
        L_model = self._model.calcL(X)
        #gndAtomIdx一個一個に対し。gibbsサンプリングを実行
        for i_data, gndAtomIdx in enumerate(self._gndAtomIdxs):
            #MLN側の尤度を計算
            l_mln = self._MLN_L0L1(state, gndAtomIdx, mln)
            #確率を計算
            p = self._softmax([0.0,l_mln + L_model[i_data]])[1]
            #サンプル
            state[gndAtomIdx] = np.random.uniform() < p

        return state

def MLN_calcL(state, gndAtomIdxs, candStates, mln):
    u"""
    単一のノードからなるgndAtom yについて、y=0とy=1の時のlog p(Y|X;MLN)を求める
    （ただし、y=0とy=1において共通の-log(Z(w))は除く）

    Li = sum_(gndFormulas which contains gndAtom)(w_j f_j(y=i))

    [Keyword arguments]
        state -- 現在のstate
        gndAtomIdxs -- 変更するatomのidxのリスト
        candStates -- boolのリストのリスト それぞれの列が上のatomに対応しており、各行が評価する状態を表す

    [Return]
        L -- 各状態において充足される論理式の重みの和
    """
    #評価すべきformulaのリスト
    gndFormulas = list(set(sum([mln.gndAtom2GndFormulas(gndAtomIdx) for gndAtomIdx in gndAtomIdxs],[])))

    #現在の値を保持しておく
    backup_val = [state[gndAtomIdx] for gndAtomIdx in gndAtomIdxs]

    Ls = []
    for i_state in xrange(len(candStates)):
        #評価する状態
        cand = candStates[i_state]
        #全てのatomに状態を展開
        for gndAtomIdx, tv in zip(gndAtomIdxs, cand):
            state[gndAtomIdx] = tv
        #充足されているweightの重みを足しあわせ
        L = 0
        for i_gndFormula in gndFormulas:
            gndFormula = mln.getGndFormula(i_gndFormula)
            weight = mln.getFormulaWeight(mln.gndFormula2Formula(i_gndFormula))

            if gndFormula.isTrue(state):
                L += weight
        #
        Ls.append(L)
        #戻す
        for gndAtomIdx, tv in zip(gndAtomIdxs, backup_val):
            state[gndAtomIdx] = tv

    return Ls


if __name__=="__main__":
    PROG_STR = """
inst={%s}

A(inst)
B(inst)

1.0 A(i) ^ B(i)
"""%",".join([str(x) for x in range(500)])
    mln = MLN(prog_str=PROG_STR)

    X = np.concatenate((
        np.random.uniform(1.0,1.0,size=250),
        np.random.uniform(-1.0,1.0,size=250)
    ))[:,np.newaxis]

    model = ModelGaussBinary(
        init_mu=np.array([[-1.0],[1.0]]),
        init_sigma=np.array([[[1.0]],[[1.0]]])
        )

    interface = BinaryModelInterface(n_inst=500, gndAtomIdxs=[mln.getGndAtomIdxByStr("B(%d)"%i) for i in range(500)]).setModel(model)

    sampler = MCSAT_Sampler(mln.mrf)
    sampler.init()

    state = [np.random.uniform() < 0.5 for n in range(mln.getNGndAtom())]

    def createEvidence(state):
        evid = {}
        for i in range(500):
            gndAtomStr = "B(%d)"%i
            gndAtomIdx = mln.getGndAtomIdxByStr(gndAtomStr)
            evid[gndAtomStr] = gndAtomIdx
        return evid

    def step1(state):
        u"""
        z1のサンプリングステップ
        """
        evidence = createEvidence(state)
        sampler.initialize(state, mln, evidence, ifEvidenceChanged=True)
        return sampler.updateState(state)

    def step2(state):
        u"""
        z2のサンプリングステップ
        """
        return interface.sample(state, mln, X)

    def updateChain(state):
        if np.random.uniform() < 0.5:
            state = step1(state)
            state = step2(state)
        else:
            state = step2(state)
            state = step1(state)
        return state

    chain = []
    for i in range(1000):
        print i
        state = updateChain(state)
        chain.append(copy.copy(state))

    print (0.5 * np.exp(1) + 0.5) / (1.0 + 0.5 + 0.5 * np.exp(1))#0.650
    chain = np.array(chain, dtype=bool)
    a_idx = [mln.getGndAtomIdxByStr("A(%d)"%i) for i in range(500)]
    print np.sum(chain[:,a_idx]) / 500.0/ 1000.0 #0.6588,0.6574, 0.6586, 0.657
